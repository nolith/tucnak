/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __TTYS_H
#define __TTYS_H

#include "header.h"
struct cwdaemon;

int ttys_init   (struct cwdaemon *);
int ttys_open   (struct cwdaemon *cwda, int verbose);
int ttys_free   (struct cwdaemon *);
int ttys_reset  (struct cwdaemon *);
int ttys_cw     (struct cwdaemon *, int onoff);
int ttys_ptt    (struct cwdaemon *, int onoff);
int ttys_cw_single(struct cwdaemon *, int onoff);
int ttys_ptt_single(struct cwdaemon *, int onoff);
int ttys_ssbway(struct cwdaemon *, int onoff);


#endif
