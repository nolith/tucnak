/*
    Tucnak - SOTA spot sender
    Copyright (C) 2002-2020  Ladislav Vaiz <ok1zia@nagano.cz>;
    2020 Michal OK2MUF

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"


#include <libzia.h>

#include "bfu.h"
//#include "dwdb.h"
//#include "edi.h"
//#include "fifo.h"
#include "main.h"
#include "menu.h"
#include "subwin.h"
#include "stats.h"
#include "tregex.h"
#include "fifo.h"

#include "sota.h"
#include "sota_upload.h"

static void sota_upload_done(struct zhttp *http);

void sota_upload(void){
    GString *gs = g_string_new("");
    GString *gs2 = g_string_new("");
    int i;
    struct band *b;
    char *c;
    int s2s = 0;

    if (!ctest) return;

    if (!ctest->pexch || strlen(ctest->pexch) == 0) 
    {
        msg_box(NULL, VTEXT(T_ERROR), AL_CENTER, TRANSLATE("SOTA Reference is empty. Go to 'Contest options' and fill 'Your EXC' as SOTA Ref."), NULL, 1, VTEXT(T_OK), NULL, B_ENTER | B_ESC);
        return;
    }

    for (i = 0; i < ctest->bands->len; i++){
        b = (struct band *)g_ptr_array_index(ctest->bands, i);
        if (b->stats->nqsos == 0) continue;
        //g_string_append_printf(gs, "%-7s  %4d  %6d\n", b->pband, b->stats->nqsos, b->stats->ntotal);
        g_string_append_printf(gs, "%-7s  %4d\n", b->pband, b->stats->nqsos);
        s2s += b->stats->nexcp;
    }

    
    g_string_append_printf(gs2, TRANSLATE("Call: %s\n"), ctest->pcall);
    g_string_append_printf(gs2, TRANSLATE("SOTA ref: %s\n"), ctest->pexch);
    g_string_append_printf(gs2, TRANSLATE("S2S count: %d\n\n"), s2s);
    g_string_append_printf(gs2, TRANSLATE("User: %s\n\n"), cfg->sota_user);
    //g_string_append_printf(gs2, "%-7s  %-4s  %-6s\n", "Band", "QSOs", "Points"); 
    g_string_append_printf(gs2, "%-7s  %-4s\n", "Band", "QSOs"); 
    g_string_insert(gs, 0, gs2->str);
    g_string_free(gs2, TRUE);
    g_string_append_printf(gs, TRANSLATE("\nUpload report to www.emuf.cz/sotalog?"));


    c = g_strdup(gs->str);

    msg_box(getml(c, NULL), VTEXT(T_INFO), AL_LEFT, 
        c, NULL, 2, 
        VTEXT(T_YES), sota_do_upload, B_ENTER,
        VTEXT(T_NO), NULL, B_ESC);

    g_string_free(gs, TRUE);
}

void sota_do_upload(void *arg){

    gchar *filename;
    struct zhttp *zhts;
    zhts = zhttp_init();
    filename = get_sota_log_filename();

    export_all_bands_sota();

    zhttp_post_free(zhts);
    zhttp_post_add_file_disk(zhts,"sotalog", "sotalog.csv", filename);
    zhttp_post_add(zhts, "action", "store");
    zhttp_post_add(zhts, "call", ctest->pcall);
    zhttp_post_add(zhts, "ref", ctest->pexch);
    zhttp_post_add(zhts, "date", ctest->cdate);
    // TODO: switch to HTTPS
    //zhttp_post(zhts, zsel, "http://192.168.11.181/work/sotalog/upload.php",sota_upload_done, NULL);
    zhttp_post(zhts, zsel, "http://www.emuf.cz/sotalog/upload.php",sota_upload_done, NULL);

    g_free(filename);    
    
}

static void sota_upload_done(struct zhttp *http){

    if (http->errorstr)
    {
        log_addf("SOTALog upload err: %s", http->errorstr);
    }
    else
    {
        log_addf("SOTA Log uploaded to http://www.emuf.cz/sotalog");
    }

    zhttp_post_free(http);
    zhttp_free(http);
}


