/*
    Tucnak - VHF contest log
    Copyright (C) 2016 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "bfu.h"
#include "inputln.h"
#include "main.h"
#include "menu.h"
#include "rain.h"
#include "rc.h"
#include "tsdl.h"
#include "terminal.h"

int rain_enable, rain_meteox, rain_wetteronline, rain_chmi, rain_weatheronline, rain_rainviewer, rain_debug;
int rain_maxqrb, rain_minscpdist, rain_mincolor;
char rain_maxqrb_str[EQSO_LEN], rain_minscpdist_str[EQSO_LEN], rain_mincolor_str[EQSO_LEN];

char *rain_colors[EQSO_LEN] = {
	TRANSLATE("Orange 1"),
	TRANSLATE("Orange 2"),
	TRANSLATE("Orange 3"),
	TRANSLATE("Red 1"),
	TRANSLATE("Red 2"),
	TRANSLATE("White")
};


void rain_color_func(void *arg){
	int active;

	active = GPOINTER_TO_INT(arg);
	dbg("rain_color_func: active=%d", active);
	if (active<0 || active >= 14) return;
	rain_mincolor = active;
	safe_strncpy0(rain_mincolor_str, get_text_translation(rain_colors[rain_mincolor - 9]), MAX_STR_LEN);
	redraw_later();
}

int dlg_rain_color(struct dialog_data *dlg, struct dialog_item_data *di){
	int i, sel;
	struct menu_item *mi;

	if (!(mi = new_menu(1))) return 0;
	for (i = 9; i < 14; i++) {
		add_to_menu(&mi, rain_colors[i-9], "", "", MENU_FUNC rain_color_func, GINT_TO_POINTER(i), 0);
	}
	sel = rain_mincolor - 9;
	if (sel < 0) sel = 0;
	do_menu_selected(mi, GINT_TO_POINTER(rain_mincolor), sel);
	return 0;
}

void refresh_rain_opts(void *arg){
    STORE_INT(cfg, rain_enable);
    STORE_INT(cfg, rain_meteox);
    STORE_INT(cfg, rain_wetteronline);
    STORE_INT(cfg, rain_chmi);
    STORE_INT(cfg, rain_weatheronline);
	STORE_INT(cfg, rain_rainviewer);
	STORE_INT(cfg, rain_debug);

	STORE_SINT(cfg, rain_maxqrb);
	STORE_SINT(cfg, rain_minscpdist);
	STORE_INT(cfg, rain_mincolor);

#ifdef Z_HAVE_SDL    
	free_rain(grain);
	grain = init_rain();
#endif
	progress(NULL);
}

void menu_rain_opts(void *itdata, void *menudata){
    struct dialog *d;
    int i;

    LOAD_INT(cfg, rain_enable);
    LOAD_INT(cfg, rain_meteox);
    LOAD_INT(cfg, rain_wetteronline);
    LOAD_INT(cfg, rain_chmi);
	LOAD_INT(cfg, rain_weatheronline);
	LOAD_INT(cfg, rain_rainviewer);
    LOAD_INT(cfg, rain_debug);
	LOAD_SINT(cfg, rain_maxqrb);
	LOAD_SINT(cfg, rain_minscpdist);
	LOAD_INT(cfg, rain_mincolor);

	if (rain_mincolor < 9 || rain_mincolor > 14) rain_mincolor = 11;
	g_strlcpy(rain_mincolor_str, rain_colors[rain_mincolor - 9], EQSO_LEN);
	

    d = (struct dialog *)g_malloc(sizeof(struct dialog) + 20 * sizeof(struct dialog_item));
    memset(d, 0, sizeof(struct dialog) + 20 * sizeof(struct dialog_item));
    d->title = VTEXT(T_RAIN_OPTIONS); 
    d->fn = dlg_pf_fn;
    d->refresh = (void (*)(void *))refresh_rain_opts;
    d->y0 = 1;

    d->items[i = 0].type = D_CHECKBOX;      
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_enable;
    d->items[i].msg = CTEXT(T_ENABLE);
    d->items[i].wrap = 2;
    
	d->items[++i].type = D_CHECKBOX;
	d->items[i].gid = 0;
	d->items[i].gnum = 1;
	d->items[i].dlen = sizeof(int);
	d->items[i].data = (char *)&rain_rainviewer;
	d->items[i].msg = "RainViewer.com (global)";
	d->items[i].wrap = 1;

	d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_meteox;
    d->items[i].msg = "Meteox";
    d->items[i].wrap = 1;

    d->items[++i].type = D_CHECKBOX;      
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_wetteronline;
    d->items[i].msg = "Wetteronline (West EU)";
    d->items[i].wrap = 1;

    d->items[++i].type = D_CHECKBOX;      
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_chmi;
    d->items[i].msg = "CHMI";
    d->items[i].wrap = 1;

	d->items[++i].type = D_CHECKBOX;      
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_weatheronline;
    d->items[i].msg = "Weatheronline (EU)";
    d->items[i].wrap = 1;

	
	d->items[i].wrap++;
	d->items[++i].type = D_FIELD;
	d->items[i].fn = check_number;
	d->items[i].gid = 0;
	d->items[i].gnum = 999;
	d->items[i].dlen = 5;
	d->items[i].data = (char *)&rain_maxqrb_str;
	d->items[i].msg = TRANSLATE("Max scatterpoint distance [km]:");
	d->items[i].wrap = 1;

	d->items[++i].type = D_FIELD;
	d->items[i].fn = check_number;
	d->items[i].gid = 5;
	d->items[i].gnum = 50;
	d->items[i].dlen = 5;
	d->items[i].data = (char *)&rain_minscpdist_str;
	d->items[i].msg = TRANSLATE("Min distance between SCPs [km]:");
	d->items[i].wrap = 1;

	d->items[++i].type = D_BUTTON;
	d->items[i].gid = 0;
	d->items[i].fn = dlg_rain_color;
	d->items[i].text = rain_mincolor_str;
	d->items[i].msg = TRANSLATE("Minimum SCP intensity color: ");
	d->items[i].wrap = 1;

	d->items[i].wrap++;
	d->items[++i].type = D_CHECKBOX;
    d->items[i].gid = 0;
    d->items[i].gnum = 1;
    d->items[i].dlen = sizeof(int);
    d->items[i].data = (char *)&rain_debug;
    d->items[i].msg = "Debug images";
    d->items[i].wrap = 1;

    
//    -----------------------------
    d->items[i].wrap++;
    d->items[++i].type = D_BUTTON; 
    d->items[i].gid = B_ENTER;
    d->items[i].fn = ok_dialog;
    d->items[i].text = VTEXT(T_OK);
    
    d->items[++i].type = D_BUTTON;
    d->items[i].gid = B_ESC;
    d->items[i].fn = cancel_dialog;
    d->items[i].text = VTEXT(T_CANCEL);
    d->items[i].align = AL_BUTTONS;
    d->items[i].wrap = 1;

    d->items[++i].type = D_END;
    do_dialog(d, getml(d, NULL));

}


