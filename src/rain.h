/*
    rain.h - Rainscatter map
    Copyright (C) 2016 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#ifndef __RAIN_H
#define __RAIN_H


#define RAIN_COLORS 15

#ifdef Z_HAVE_SDL

struct zhttp;
struct rain;

struct rain_km{
	float kx, ky;     // center
	float kx1, ky1;	  // left upper
	float kx2, ky2;	  // right upper
	float kx3, ky3;	  // left lower
	float kx4, ky4;	  // right lower
};

#define RV_TILES 16

struct rain_provider{
	char *name; // const not freeed
	double h1, h2, w1, w2; // synchro points geo coordinates
	int hpx1, hpx2, wpx1, wpx2; // synchro points pixels 
	int top, left, bottom, right; // crop
	int *srccols;
	int srccolslen;
	int *translation; // index = src color, value = rain->colors index
	SDL_Surface *img;
    int *pal;

	struct rain *rain;
	int timer_id;
    struct zhttp *http;
	int w, h;
	GHashTable *hashes[RAIN_COLORS]; // of struct zbinbuf of struct rain_pixel
	gpointer (*hash_func)(short kx, short ky, int ci);
	void(*img2hw)(struct rain_provider *provider, float ix, float iy, double *h, double *w);

	int mask, step;
	double hmult, wmult;
	char *title;

	int ofs_min;

	int rv_zoom, rv_size, rv_pow, rv_mul;
	char *rv_path, *rv_oldpath;
	int rv_mypx, rv_mypy;
	float rv_px0, rv_py0;

};

struct rain_scp{
	char wwl[7];
	//double h, w;
	int kx, ky;
	int qrb;
};

struct rain{
    int *colors, *bwcolors;
	int debug;

	struct rain_provider *meteox;
	struct rain_provider *wetteronline;
	struct rain_provider *chmi;
	struct rain_provider *weatheronline;
	struct rain_provider *rainviewer;

	int maxqrb; // typ. 300km
	int mincolor; // typ. 11 (orange)
	int minscpdist; // typ 20km
	GPtrArray *scps; // of rain_scp
	GString *scplocs; // loc1 loc2 ... locn

};

extern struct rain *grain;

struct rain *init_rain(void);
void free_rain(struct rain *rain);

struct rain_scp *init_rain_scp(float kx, float ky);
void free_rain_scp(struct rain_scp *scp);
void rain_analyze_scps(struct rain *rain);
void rain_add_scps(struct rain_provider *prov);
int rain_scp_exists(struct rain_provider *prov, float kx, float ky);

void free_rain_provider(struct rain_provider *provider);
void rain_linear_img2hw(struct rain_provider *provider, float ix, float iy, double *h, double *w);
void rain_reload(void);

void rain_meteox_timer(void *arg);
void rain_meteox_downloaded(struct zhttp *http);
void rain_meteox_load(struct rain_provider *provider);
gpointer rain_meteox_hash(short px, short py, int color);

void rain_wetteronline_timer(void *arg);
void rain_wetteronline_downloaded(struct zhttp *http);
void rain_wetteronline_load(struct rain_provider *provider);
gpointer rain_wetteronline_hash(short px, short py, int color);

void rain_chmi_timer(void *arg);
void rain_chmi_downloaded(struct zhttp *http);
void rain_chmi_load(struct rain_provider *provider);
gpointer rain_chmi_hash(short px, short py, int color);

void rain_weatheronline_timer(void *arg);
void rain_weatheronline_downloaded(struct zhttp *http);
void rain_weatheronline_load(struct rain_provider *provider);
gpointer rain_weatheronline_hash(short px, short py, int color);
void rain_weatheronline_img2hw(struct rain_provider *provider, float ix, float iy, double *h, double *w);

void rain_rainviewer_init(struct rain_provider *provider);
int tile_googlezoom(double dh, double dw);
void rain_rainviewer_timer(void *arg);
void rain_rainviewer_downloaded_json(struct zhttp *http);
void rain_rainviewer_process_json(struct rain_provider*, const char *json);
void rain_rainviewer_downloaded_image(struct zhttp *http);
void rain_rainviewer_load(struct rain_provider *provider);
gpointer rain_rainviewer_hash(short kx, short ky, int ci);
void rain_rainviewer_hw2himg(struct rain_provider *provider, double h, double w, int *x, int *y);
void rain_rainviewer_img2hw(struct rain_provider *provider, float x, float y, double *h, double *w);




#endif
#endif
		
