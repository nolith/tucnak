/*
    sdrd.h - Header for SDR daemon
    Copyright (C) 2016 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SDRD_H
#define __SDRD_H

#define SDRD_TCP_PORT 7300

#define SDRD_CMD_SETUP 1
#define SDRD_CMD_DATA 2

#ifdef Z_MSC
#pragma pack(push, 1)
#endif

struct sdrd_header{
	uint16_t cmd;
	uint16_t len;  // length of data without header
	// data here
}__attribute__((packed));

struct sdrd_setup{
	struct sdrd_header hdr;
	int32_t speed, channels, frames;
}__attribute__((packed));

#ifdef Z_MSC
#pragma pack(pop)
#endif


#endif
