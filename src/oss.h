/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __OSS_H
#define __OSS_H

#include "header.h"
#include "dsp.h"

#ifdef HAVE_OSS
#ifdef HAVE_SNDFILE
int oss_set_format(struct dsp *dsp, SF_INFO *sfinfo, int rec);
#endif
int oss_open(struct dsp *dsp, int rec);
int oss_close(struct dsp *dsp);
int oss_write(struct dsp *dsp, void *data, int frames);
int oss_read(struct dsp *dsp, void *data, int frames);
int oss_reset(struct dsp *dsp);
int oss_sync(struct dsp *dsp);
int oss_get_sources(struct dsp *dsp, GString *labels);
int oss_set_source(struct dsp *dsp);
int oss_set_plevel(struct dsp *dsp);
char *oss_recsrc2source(int recsrc);

#endif


#endif
