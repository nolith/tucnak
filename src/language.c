/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2006  Ladislav Vaiz <ok1zia@nagano.cz>
    and authors of web browser Links 0.96

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "charsets.h"
#include "codepage.h"
#include "language.h"
#include "tsdl.h"
#include "terminal.h"

struct translation {
    int code;
    char *name;
};

struct translation_desc {
    struct translation *t;
};

char dummyarray[T__N_TEXTS];

#ifdef Z_ANDROIDxtest
#include "langascii.inc"
#else
#include "language.inc"
#endif

static char **translation_array[N_LANGUAGES][N_CODEPAGES];

int current_language;
static int current_lang_charset;

void init_trans(void)
{
    int i, j;
    for (i = 0; i < N_LANGUAGES; i++)
        for (j = 0; j < N_CODEPAGES; j++)
            translation_array[i][j] = NULL;
    current_language = 0;
    current_lang_charset = 0;
    translations[0].t=translation_english;
}

void shutdown_trans(void)
{
    int i, j, k;
    for (i = 0; i < N_LANGUAGES; i++){
        for (j = 0; j < N_CODEPAGES; j++) {
            if (translation_array[i][j]) {
                for (k = 0; k < T__N_TEXTS; k++) {
                    if (translation_array[i][j][k]) g_free(translation_array[i][j][k]);
                }
                g_free(translation_array[i][j]);
            }
        }
    }
}


char *get_text_translation(char *text)
{
    char **current_tra;
    char *trn = NULL;

    //dbg("get_text_translation('%s') spec=%p\n", text, term->spec);
    //dbg("get_text_translation('%s') charset=%d\n", text, term->spec->charset);

    if (text < dummyarray || text > dummyarray + T__N_TEXTS) return text;
    //dbg("--------get_text_translation(%d)\n", text - dummyarray);
    if ((current_tra = translation_array[current_language][term->spec->charset])) {
        if ((trn = current_tra[text - dummyarray])) return trn;
        tr:
            trn = g_strdup(translation_english[text - dummyarray].name);
        current_tra[text - dummyarray] = trn;
    } else {
        if (current_lang_charset && term->spec->charset != current_lang_charset) {
            if ((current_tra = translation_array[current_language][term->spec->charset] = (char **)g_malloc(sizeof (char **) * T__N_TEXTS))) {
                memset(current_tra, 0, sizeof (char **) * T__N_TEXTS);
                goto tr;
            }
        }
        if (!(trn = translations[current_language].t[text - dummyarray].name)) {
            trn = translations[current_language].t[text - dummyarray].name = translation_english[text - dummyarray].name;   /* modifying translation structure */
        }
    }
        //dbg("trn=%p\n", trn);
        //dbg("trn='%s'", trn);
        //dbg("return\n");
    return trn;
}

char *get_english_translation(char *text)
{
    if (text < dummyarray || text > dummyarray + T__N_TEXTS) return text;
    return translation_english[text - dummyarray].name;
}

int n_languages(void)
{
    return N_LANGUAGES;
}

char *language_name(int l)
{
    return translations[l].t[T__LANGUAGE].name;
}

void set_language(int l)
{
    int i;
    char *cp;
    dbg("set_language charset=%d language=%d\n", current_lang_charset, current_language);
    for (i = 0; i < T__N_TEXTS; i++){
    //    dbg("  i=%d ;l=%d; tr[..]=%d \n", i,l,translations[l].t[i].code);
        if (translations[l].t[i].code != i) {
            zinternal("Bad table for language %s (%d!=%d). Run script fixlang.pl or maybe add this language into this script.", 
                    translations[l].t[T__LANGUAGE].name,
                    translations[l].t[i].code, i);
            return;
        }
    }
    current_language = l;
    cp = translations[l].t[T__CHAR_SET].name;
    dbg("codepage=%s\n", cp);
    i = get_cp_index(cp);
    if (i == -1) {
        zinternal("Unknown charset for language %s.", translations[l].t[T__LANGUAGE].name);
        i = 0;
    }
    current_lang_charset = i;
    dbg("return charset=%d language=%d\n", current_lang_charset, current_language);
#ifdef Z_HAVE_SDL
    if (sdl && term && term->spec){
        term->spec->charset = i;
    }
#endif
}
