/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __DAVAC4_H
#define __DAVAC4_H

#ifdef Z_HAVE_LIBFTDI
int davac4_init    (struct cwdaemon *);
int davac4_open    (struct cwdaemon *cwda, int verbose);
int davac4_free    (struct cwdaemon *);
int davac4_reset   (struct cwdaemon *);
int davac4_cw      (struct cwdaemon *, int onoff);
int davac4_ptt     (struct cwdaemon *, int onoff);
int davac4_ssbway  (struct cwdaemon *, int onoff);
int davac4_monitor (struct cwdaemon *, int onoff);
int davac4_band    (struct cwdaemon *, int bandsw);


/*#ifndef HAVE_FTDI_NEW
struct ftdi_context *ftdi_new(void);
void ftdi_free(struct ftdi_context *ftdi);
#endif*/

void usb_info(GString *gs);
unsigned short ftdi_checksum(unsigned char *eeprom, int eeprom_size);

#endif /* Z_HAVE_LIBFTDI */ 

#endif
