/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2012  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/
#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif
#include <zconfig.h>

#ifdef Z_HAVE_SDL
#include "libzia.h"
#include "cordata.h"
#include "cordata.inc"
#endif

