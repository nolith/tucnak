/*
    Tucnak - VHF contest log
    Copyright (C) 2013 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __STATE_H
#define __STATE_H

#include "header.h"

#ifdef Z_ANDROID
#include <jni.h>
#endif

struct state{
	GHashTable *hash;	
#ifdef Z_ANDROID
    JNIEnv *env;
#endif
};

struct state *init_state(void);
void free_state(struct state *);

void state_test(struct state *state);
void state_save(struct state *state);
void state_restore(struct state *state);

void state_putstr(struct state *state, char *key, char *val);
void state_dupstr(struct state *state, const char *key, const char *val);
const char *state_getstr(struct state *state, const char *key);
void state_foreach(struct state *state, GHFunc func, gpointer user_data);


#endif
