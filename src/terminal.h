
/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2015 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __TERMINAL_H
#define __TERMINAL_H

#include "header.h"

/* UTF-8 */
typedef uint32_t unicode_val;
/* UCS/Unicode replacement character. */
#define UCS_NO_CHAR ((unicode_val) 0xFFFD)

typedef unsigned short chr;

struct event {
    long ev;
    long x;
    long y;
    long b;
    long mx;
    long my;
    double vx;
    double vy;
};

#define EV_INIT         0
#define EV_KBD          1
#define EV_MOUSE        2
#define EV_REDRAW       3
#define EV_RESIZE       4
#define EV_ABORT        5
#define EV_SDLRESIZE    6
#define EV_SKIP         7

struct window {
    struct window *next;
    struct window *prev;
    void (*handler)(struct window *, struct event *, int fwd);
    void *data;
    int xp, yp;
    struct terminal *term;
};

#define MAX_TERM_LEN    64  /* this must be multiple of 8! (alignment problems) */

#define MAX_CWD_LEN 8192    /* this must be multiple of 8! (alignment problems) */  

#define ENV_XWIN    1
#define ENV_SCREEN  2
#define ENV_OS2VIO  4
#define ENV_BE      8
#define ENV_TWIN    16
#undef FALL

/*

screen:
bit0 - bit7        0x000000ff character
bit8 - bit10       0x00000700 foreground
bit11 - bit13      0x00003800 background
bit14              0x00004000 brightness
bit19              0x00080000 doubleht
bit20              0x00100000 doublehb


*/

struct terminal {
    int master;
    int fdin;
    int fdout;
    int x;
    int y;
    int environment;
    char term[MAX_TERM_LEN];
    unsigned *screen;
    unsigned *last_screen;
    struct term_spec *spec;
    int cx;
    int cy;
    int lcx;
    int lcy;
    int dirty;
    int redrawing;
    int blocked;
    char *input_queue;
    int qlen;
    /*struct list_head windows;*/
    struct window  windows;
     char *title;
    /* Something weird regarding the UTF8 I/O. */
    struct {
        unicode_val ucs;
        int len;
        int min;
    } utf_8;
#ifdef FALL
    int *fall;
#endif
};

extern struct terminal *term;

struct term_spec {
    struct term_spec *next;
    struct term_spec *prev;
    char term[MAX_TERM_LEN];
    int mode;
    int m11_hack;
    int restrict_852;
    int block_cursor;
    int col;
    int utf_8_io;
    int charset;
};

#define TERM_DUMB   0
#define TERM_VT100  1
#define TERM_LINUX  2
#define TERM_KOI8   3

#define ATTR_FRAME  0x8000

extern struct term_spec term_specs;

int hard_write(int, char *, int);
int hard_read(int, char *, int);
char *get_cwd(void);
void set_cwd(char *);
struct terminal *init_term(int, int, void (*)(struct window *, struct event *, int));
void sync_term_specs(void);
struct term_spec *new_term_spec(char *);
void free_term_specs(void);
void destroy_terminal(void *);
void redraw_terminal(void *);
void redraw_terminal_all(void);
void redraw_terminal_cls(void);
void redraw_later(void);
/*void cls_redraw_all_terminals(void);*/
void redraw_from_window(struct window *);
void redraw_below_window(struct window *);
void add_window(void (*)(struct window *, struct event *, int), void *);
void add_window_at_pos(void (*)(struct window *, struct event *, int), void *, struct window *);
void delete_window(struct window *);
void delete_window_ev(struct window *, struct event *ev);
void set_window_ptr(struct window *, int, int);
void get_parent_ptr(struct window *, int *, int *);
struct window *get_root_window(void);
/*void add_empty_window(void (*)(void *), void *);*/
void term_redraw_screen(void);
/*void redraw_all_terminals(void);*/
void set_char(int, int, unsigned);
void set_last_char(int, int, unsigned);
unsigned get_char(int, int);
void set_color(int, int, unsigned);
void set_only_char(int, int, unsigned);
void set_line(int, int, int, chr *);
void set_line_color(int, int, int, unsigned);
void fill_area(int x, int y, int xw, int yw, unsigned c);
void fill_lastarea(int x, int y, int xw, int yw, unsigned c);
void draw_frame(int, int, int, int, unsigned, int);
void print_text(int, int, int, char *, unsigned); /* todo, ale asi nikoli jen smazat term */
void set_cursor(int, int, int, int);
/*void destroy_all_terminals(void);*/
void block_itrm(int);
int unblock_itrm(int);
void exec_thread(char *, int);
void close_handle(void *arg);
void set_ctest_title(void);

#define TERM_FN_TITLE   1
#define TERM_FN_RESIZE  2

void exec_on_terminal(char *, char *, int);
void set_terminal_title(char *);
void do_terminal_function(char, char *);
void timer_redraw(void *);


#define FRAME_URDL 0x80c5
#define FRAME_UDL  0x80b4
#define FRAME_URD  0x80c3
#define FRAME_URL  0x80c1
#define FRAME_RDL  0x80c2
#define FRAME_RL   0x80c4

#endif
