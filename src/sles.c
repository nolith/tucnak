/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2013  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "dsp.h"

#ifdef HAVE_SLES_OPENSLES_H
#include <SLES/OpenSLES_Android.h>
#endif


int sles_open(struct dsp *dsp, int rec){
    int result;

    result = slCreateEngine(&(dsp->engineObject), 0, NULL, 0, NULL, NULL);

    if (!rec){
        // play
        const SLInterfaceID ids[] = {SL_IID_VOLUME};
        const SLboolean req[] = {SL_BOOLEAN_FALSE} ;
        result = (*dsp->engineEngine)->CreateOutputMix(dsp->engineEngine, &(dsp->outputMixObject), 1, ids, req);
        result = (*dsp->outputMixObject)->Realize(dsp->outputMixObject, SL_BOOLEAN_FALSE);

        SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
        SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, dsp->channels, dsp->speed, SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
               speakers, SL_BYTEORDER_LITTLEENDIAN};
        SLDataSource audioSrc = {&loc_bufq, &format_pcm};

        SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, dsp->outputMixObject};
        SLDataSink audioSnk = {&loc_outmix, NULL};

        const SLInterfaceID ids1[] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE};
        const SLboolean req1[] = {SL_BOOLEAN_TRUE};
        result = (*dsp->engineEngine)->CreateAudioPlayer(dsp->engineEngine, &(dsp->bqPlayerObject), &audioSrc, &audioSnk, 1, ids1, req1);
        result = (*dsp->bqPlayerObject)->Realize(dsp->bqPlayerObject, SL_BOOLEAN_FALSE);

        result = (*dsp->bqPlayerObject)->GetInterface(dsp->bqPlayerObject, SL_IID_PLAY,&(dsp->bqPlayerPlay));
        result = (*dsp->bqPlayerObject)->GetInterface(dsp->bqPlayerObject, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, &(dsp->bqPlayerBufferQueue));

        result = (*dsp->bqPlayerBufferQueue)->RegisterCallback(dsp->bqPlayerBufferQueue, bqPlayerCallback, dsp);
        result = (*p->bqPlayerPlay)->SetPlayState(p->bqPlayerPlay, SL_PLAYSTATE_PLAYING);

    }else{
        // record

        SLDataLocator_IODevice loc_dev = {SL_DATALOCATOR_IODEVICE, SL_IODEVICE_AUDIOINPUT, SL_DEFAULTDEVICEID_AUDIOINPUT, NULL};
        SLDataSource audioSrc = {&loc_dev, NULL};

        SLDataLocator_AndroidSimpleBufferQueue loc_bq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
        SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, channels, sr, SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
              speakers, SL_BYTEORDER_LITTLEENDIAN};
        SLDataSink audioSnk = {&loc_bq, &format_pcm};

        const SLInterfaceID id[1] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE};
        const SLboolean req[1] = {SL_BOOLEAN_TRUE};
        result = (*p->engineEngine)->CreateAudioRecorder(p->engineEngine, &(p->recorderObject), &audioSrc, &audioSnk, 1, id, req);

        result = (*p->recorderObject)->Realize(p->recorderObject, SL_BOOLEAN_FALSE);

        result = (*p->recorderObject)->GetInterface(p->recorderObject, SL_IID_RECORD, &(p->recorderRecord));

        result = (*p->recorderObject)->GetInterface(p->recorderObject, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, &(p->recorderBufferQueue));

        result = (*p->recorderBufferQueue)->RegisterCallback(p->recorderBufferQueue, bqRecorderCallback,p);

        result = (*p->recorderRecord)->SetRecordState(
                      p->recorderRecord,SL_RECORDSTATE_RECORDING);
    }

}

int sles_close(struct dsp *dsp){
}



int sles_write(struct dsp *dsp, void *data, int len){
}

int sles_read(struct dsp *dsp, void *data, int len){
}

int sles_reset(struct dsp *dsp){
}

int sles_sync(struct dsp *dsp){
}

int sles_set_format(struct dsp *dsp, SF_INFO *sfinfo, int ret){
}
