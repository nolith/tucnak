/*
    sdrc.h - headers for SDR client
    Copyright (C) 2016 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SDRC_H
#define __SDRC_H

#include "header.h"
#include "dsp.h"

int sdrc_open(struct dsp *dsp, int rec);
int sdrc_close(struct dsp *dsp);
int sdrc_write(struct dsp *dsp, void *data, int frames);
int sdrc_read(struct dsp *dsp, void *data, int frames);
int sdrc_reset(struct dsp *dsp);
int sdrc_sync(struct dsp *dsp);
#ifdef HAVE_SNDFILE
int sdrc_set_format(struct dsp *dsp, SF_INFO *sfinfo, int rec);
#endif
int sdrc_set_sdr_format(struct dsp *dsp, int frames, int speed, int rec);


#endif
