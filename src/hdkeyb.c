/*
    hdkeyb.c - routines for hdkeyb control
    http://ok1zia.nagano.cz/wiki/Hdkeyb

    Copyright (C) 2010-2015 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#include "ac.h"
#include "fifo.h"
#include "hdkeyb.h"
#include "language2.h"
#include "main.h"
#include "rotar.h"
#include "sdev.h"
#include "terminal.h"
#include "tsdl.h"


struct hdkeyb *hdkeyb;

struct hdkeyb *init_hdkeyb(void){
    struct hdkeyb *hdkeyb;

	progress(VTEXT(T_INIT_ROTAR));		
    hdkeyb = g_new0(struct hdkeyb, 1);
    hdkeyb->zhdkeyb = zhdkeyb_init(zsel);
    hdkeyb_draw_rotars(hdkeyb);          
    return hdkeyb;
}

void free_hdkeyb(struct hdkeyb *hdkeyb){
    if (!hdkeyb) return;
	progress(VTEXT(T_FREE_ROTAR));		

    zhdkeyb_free(hdkeyb->zhdkeyb);
    g_free(hdkeyb);
}


void hdkeyb_read_handler(int n, char **items){
    char *cmd, *key;

    if (!hdkeyb) return;
    
    cmd = items[1];
     //dbg("hdkeyb_read_handler   rcvd: '%s'  cmd='%s'\n", s, cmd);
    if (strcmp(cmd, "!")==0){  /* error */
        log_addf("Hdkeyb error: %s %s", items[2], items[3]);
    }

    if (strcasecmp(cmd, "k")==0){
		key=items[2];
        hdkeyb_key(hdkeyb, key[0]);
    }
    redraw_later();
}

void hdkeyb_key(struct hdkeyb *hdkeyb, char key){
    //dbg("Hdkeyb key='%c'\n", key);
    int len = strlen(hdkeyb->qtfstr);

    switch (key){
        case 'A':
            hdkeyb_activate(hdkeyb, 0);
            return;
        case 'B':
            hdkeyb_activate(hdkeyb, 1);
            return;
    }

    if (len == 0){
        switch(key){
            case 'C':
                hdkeyb_activate(hdkeyb, 2);
                return;
            case 'D':
                hdkeyb_activate(hdkeyb, 3);
                return;
            case '0':
            case '1':
            case '2':
            case '3':
                hdkeyb->qtfstr[len++] = key;
                hdkeyb->qtfstr[len] = '\0';
                break;
            case '4':
                hdkeyb_rel_qtf(hdkeyb, -5);
                return;
            case '6':
                hdkeyb_rel_qtf(hdkeyb, +5);
                return;
            case '7':
                hdkeyb_rel_qtf(hdkeyb, -10);
                return;
            case '9':
                hdkeyb_rel_qtf(hdkeyb, +10);
                return;
            case '*':
                hdkeyb_rel_qtf(hdkeyb, -2);
                return;
            case '#':
                hdkeyb_rel_qtf(hdkeyb, +2);
                return;
        }
    }else{
        switch(key){
            case 'C':
                hdkeyb->qtfstr[len - 1] = '\0';
                return;
            case 'D':
                hdkeyb_set_qtf(hdkeyb);
                return;
        }
        if (key >= '0' && key <= '9'){
            hdkeyb->qtfstr[len++] = key;
            hdkeyb->qtfstr[len] = '\0';
        }
    }
    
    len = strlen(hdkeyb->qtfstr);
    if (len == 3) hdkeyb_set_qtf(hdkeyb);
    hdkeyb_draw_rotar(hdkeyb, hdkeyb->actnr);
}

void hdkeyb_activate(struct hdkeyb *hdkeyb, int nr){
    int oldnr;

	dbg("hdkeyb_activate(%d) old=%d\n", nr, hdkeyb->actnr);
    oldnr = hdkeyb->actnr;
    hdkeyb->actnr = nr;
    hdkeyb_draw_rotar(hdkeyb, oldnr);
    hdkeyb_draw_rotar(hdkeyb, nr);
    hdkeyb->qtfstr[0] = '\0';
}

void hdkeyb_set_qtf(struct hdkeyb *hdkeyb){
    struct rotar *rot;
    int qtf;

    rot = get_rotar(hdkeyb->actnr);
    if (rot){
        qtf = atoi(hdkeyb->qtfstr);
		ac_track(gacs, NULL, -10);
        rot_seek(rot, qtf, -90);
		dbg("******** rot_seek(%c, %d, %03d)\n", rot->rotchar, rot->sdev->saddr, qtf);
    }
    strcpy(hdkeyb->qtfstr, "");
}

void hdkeyb_rel_qtf(struct hdkeyb *hdkeyb, int rel){
    struct rotar *rot;
    int qtf;

    rot = get_rotar(hdkeyb->actnr);
    if (!rot) return;

    qtf = rot->qtf + rel;
	ac_track(gacs, NULL, -10);
	rot_seek(rot, qtf, -90);
}

void hdkeyb_draw_rotar(struct hdkeyb *hdkeyb, int nr){
    int li, co;
	struct rotar *rot;

    if (!hdkeyb) return;
    rot = get_rotar(nr);
    if (!rot) return;

    li = nr / 2;
    co = (nr % 2) * Z_HDKEYB_CHARS / 2;
//	dbg("nr=%d li=%d co=%d\n", nr, li, co);

    if (nr == hdkeyb->actnr){
        if (strlen(hdkeyb->qtfstr) > 0){
            zhdkeyb_printf(hdkeyb->zhdkeyb, li, co, "\x02%c%3s\x01\x03", 
                    'A' + nr,
                    hdkeyb->qtfstr);
        }else{
            zhdkeyb_printf(hdkeyb->zhdkeyb, li, co, "\x02%c%03d\x01\x03", 
                    'A' + nr,
                    rot->qtf);
        }
    }else{
        zhdkeyb_printf(hdkeyb->zhdkeyb, li, co, " %c%03d\x01 ", 
                'A' + nr,
                rot->qtf);
    }


}

void hdkeyb_draw_rotars(struct hdkeyb *hdkeyb){
    int i;
//	return;
    for (i=0; i < Z_HDKEYB_LINES * 2; i++) 
        hdkeyb_draw_rotar(hdkeyb, i);          

	//hdkeyb_dump_vrams(hdkeyb);
}

#if 0
int hdkeyb_test(void){
    struct hdkeyb *hdkeyb;
    int i;

    dbg("\n\n\n");
    hdkeyb = init_hdkeyb();
    if (!hdkeyb) {
        error("init_hdkeyb() failed\n");
        return -1;
    }
    hdkeyb_setdir(hdkeyb);
    hdkeyb_reset(hdkeyb);
    usleep(300000);

/*//    dbg("1 %x\n", hdkeyb->rd);
    hdkeyb->wr = 0x55;
    WRITE;
//    dbg("2 %x\n", hdkeyb->rd);
    hdkeyb->wr = 0xaa;
    WRITE;
//    dbg("3 %x\n", hdkeyb->rd);
    READ;
//    dbg("4 %x\n", hdkeyb->rd);
    READ;
    READ;
//    dbg("5 %x\n", hdkeyb->rd);
    hdkeyb_free(hdkeyb);
    */

/*    while(1){
        int a= hdkeyb_key(hdkeyb);
        if (a==0) continue;
        dbg("%c\n", a);
    } */
//  gst_start();
//    hdkeyb_print2(hdkeyb, 0, "ABC");
    char ss[50];
    strcpy(ss, "");
    for (i=0; i<100000000; i++){
        char s[50];
        sprintf(s, "jedeme %d", i);
        hdkeyb_setdir(hdkeyb);
        hdkeyb_print(hdkeyb, 80, s);
        
        int a= hdkeyb_key(hdkeyb);
        if (a==0) continue;
        if (a==-1) continue;

        dbg("%d\n", a);
        switch(a){
            case 'C':
                if (strlen(ss)==0) break;
                ss[strlen(ss)-1]='\0';
                break;
            default:
                if (strlen(ss)==15) break;
                ss[strlen(ss)+1]='\0';
                ss[strlen(ss)]=a;
                break;
        }
        sprintf(s, "%-16s", ss);
        hdkeyb_print(hdkeyb, 16, s);
        
    }
//  gst_stop();
    return 0;
}
#endif
