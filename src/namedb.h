/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __NAMEDB_H
#define __NAMEDB_H

struct namedb{
    GHashTable *names; /* key=rawcall, value=name */
	int dirty;
    //GHashTable *dummy;
};

extern struct namedb *namedb;  
  
struct namedb *init_namedb(void);
void free_namedb(struct namedb *namedb);
void read_namedb_files(struct namedb *namedb);
gchar *find_name_by_call(struct namedb *namedb, gchar *call);
int save_namedb_into_file(struct namedb *namedb, gchar *filename);
void add_namedb(struct namedb *namedb, gchar *call, gchar *name);
gint get_namedb_size(struct namedb *namedb);
int save_namedb(struct namedb *namedb, int verbose);
int load_namedb_from_mem(struct namedb *namedb, const char *file, size_t len);
int format_namedb_string(struct namedb *namedb, GString *gs);
void load_one_namedb(struct namedb *namedb, gchar *s);



#endif
