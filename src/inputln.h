/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __INPUTLN_H
#define __INPUTLN_H

#include "header.h"
#include "bfu.h"

#define VALID_CHARS " ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/#',.+-_:?!;*"

struct inputln {
    int (*fn)(struct dialog_data *, struct dialog_item_data *);
    int dlen;           /* data length */
    int x, y, l;
    int vpos, cpos, focused, wasctrlv;
    int upconvert, readonly, allow_ctrlv;
    char *cdata;
    struct terminal *term;
    void *enterdata;
    void (*enter)(void *, gchar *, int);
    GPtrArray *history;
    int hist_i;
    struct band *band;
    struct subwin *sw;
    char *valid_chars;
};

void il_add_to_history(struct inputln *il, char *s);
void il_set_focus(struct inputln *il);
void il_unset_focus(struct inputln *il);
void clear_inputline(struct inputln *il);
int inputln_func(struct inputln *il, struct event *ev);
void il_readonly(struct inputln *il, int ro);
void draw_inputln(struct inputln *il, int sel);
void newkst(struct inputln *il, char *callkst);
void askkst(struct inputln *il, char *s);


#endif
