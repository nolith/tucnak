/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SKED_H
#define __SKED_H

#include "header.h"

struct dialog_data;
struct dialog_item_data;
struct event;
struct subwin;
struct qso;
struct tmpqso;
struct window;

struct skedwin_data{
    int x,y,w,h;
    int esccnt;
    struct band *band;
};

struct sked{
    gchar *sked_time, *operator_ , *src_shortpband, *qrg;
    gchar *pband, *time_str, *callsign, *locator, *remark;

    int worked;
};


int sw_sked_kbd_func(struct subwin *sw, struct event *ev, int fw);
int sw_sked_mouse_func(struct subwin *sw, struct event *ev, int fw);
gchar *sw_sked_sprint(struct sked *sked);
void sw_sked_redraw(struct subwin *sw, struct band *band, int flags);
void sw_sked_check_bounds(struct subwin *sw);
void sw_sked_read(gchar *str, int from_my);
void refresh_sked(struct qso *q);
void sked_fn(struct dialog_data *dlg);
void sked(void);
void sked_from_tmpqso(struct tmpqso *tmpqso);
void sked_from_qso(struct qso *qso);
void sked_pband_func(void *arg);
void sked_pband(int apband_int);
int dlg_pband(struct dialog_data *dlg, struct dialog_item_data *di);
struct zstring *sked_format(struct sked *sked);
struct sked *sked_parse(struct zstring *zstr);
void free_sked(struct sked *sked);
void recalc_worked_skeds(struct band *band);
void draw_skedwindow(struct skedwin_data *skedwdata);
void skedwindow_func(struct window *win, struct event *ev, int fwd);
void skedw_create(struct band *b);




#endif
