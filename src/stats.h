/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2021 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __STATS_H
#define __STATS_H

#include "header.h"
struct band;
struct stats;
struct qso;


struct cntpts{
	int count;
	int points;
    struct qso *qso; /* used only in st->hours */
};

struct stats{
	char bandchar;

    gint first_date, last_date; /* 20021115*/
    gint nqsos, nqsop;  /* without errors and dupes */
    gint nwwlp; 
    gint nprefs; 
    gint ndxcp; 
    gint nexcp; 
    gint ntotal;
    int nmult;

    gint odxqrb_int;
    gchar *odxcall, *odxwwl, *odxoperator;

    GHashTable *wwls, *dxcs, *excs, *hours, *prefs; /* of struct cntpts */
	GHashTable *valid;
    
	int qso_per_hour, pts_per_hour, pts_per_50qso;
    char *mycontinent;
	char *mydxcc;
	struct qso *odx;
	int oldphase;
};

struct miss_struct{
    ZPtrArray *ia;
    struct stats *st;
    int maxlen;
};


struct contest;
struct config_subwin;

char *get_wwl(char *buf, char *wwl);

void draw_one_bigdigit(int x, int y, int num);
void draw_bigdigit(int x, int y, int num);

struct stats *init_stats(char bandchar);
void free_stats(struct stats *st);
void clear_stats(struct stats *st);
void update_stats(struct band *b, struct stats *st, struct qso *q);
void recalc_stats(struct band *b);
void recalc_all_stats(struct contest *ctest);
void recalc_all_qrbqtf(struct contest *ctest);
void redraw_stats(struct band *band);
void recalc_statsfifo(struct band *band);
void export_stats_fifo(void);

void stats_thread_create(struct band *band);
void stats_thread_join(struct band *band);
void stats_thread_kill(struct band *band);

void timer_minute_stats_all(void *arg);
void minute_stats_all(void);
void minute_stats(struct band *band);

void stats_merge_prefs_fn(gpointer key, gpointer value, gpointer data);
void recalc_allb_stats(void);
void miss_compute_ms(struct miss_struct *ms, struct band *b);
int qsomult16(struct band *b);
char *stats_valid_key(char *buf, const char *call, const char *wwl, const int phase, const enum modes mode);
char *stats_valid_key_display(char *buf, const char *call, const char *wwl, const int phase, const enum modes mode);
struct qso *stats_get_dupe(struct stats *st, const char *call, const char *wwl, const int phase, const enum modes mode);

#endif
