/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __HDKEYB_H
#define __HDKEYB_H

struct zhdkeyb;

struct hdkeyb{
    struct zhdkeyb *zhdkeyb;
    int actnr;
    char qtfstr[4];
};

extern struct hdkeyb *hdkeyb;

struct hdkeyb *init_hdkeyb(void);
void free_hdkeyb(struct hdkeyb *hdkeyb);

void hdkeyb_read_handler(int n, char **items);
void hdkeyb_key(struct hdkeyb *hdkeyb, char key);
void hdkeyb_activate(struct hdkeyb *hdkeyb, int nr);

void hdkeyb_set_qtf(struct hdkeyb *hdkeyb);
void hdkeyb_rel_qtf(struct hdkeyb *hdkeyb, int rel);

void hdkeyb_draw_rotar(struct hdkeyb *hdkeyb, int nr);
void hdkeyb_draw_rotars(struct hdkeyb *hdkeyb);
#endif
