/*
    Tucnak - SOTA log upload
    Copyright (C) 2002-2020  Ladislav Vaiz <ok1zia@nagano.cz>;
    2020 Michal OK2MUF

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SOTA_UPLOAD_H
#define __SOTA_UPLOAD_H

void sota_upload(void);
void sota_do_upload(void *arg);

#endif
