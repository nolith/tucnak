/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This pmrogram is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __FFT_H
#define __FFT_H

#include "header.h"

#ifdef USE_SDR


#include "ssbd.h"
#include "subwin.h"
#include <fftw3.h>

#define BLUEPALx
#define ZIAPAL

#ifdef ZIAPAL
#define FFT_COLORS 864
#endif
#ifdef BLUEPAL
#define FFT_COLORS 256
#endif
#define FFT_AMPLEN (SSBBUFFER_LEN2/4 + 1)
#define FFT_MA 8

	
struct fft{
    fftw_plan plan;
    double *allocrin, *dynrin;
    fftw_complex *alloccout, *dyncout;
	size_t sizerin, sizecout;
	int lenrin, lencout;
    int n;
    int samplerate;
    int pal[FFT_COLORS];
#ifdef Z_HAVE_SDL    
    SDL_Surface *screen;
    MUTEX_DEFINE(screen);
    int screeny; 
#endif   
    double *amp;
	int amplen;
#ifdef USE_NEA
    double nea_amp[FFT_AMPLEN];
#endif
};        	

extern struct fft *gfft;

struct fft *init_fft(void);
void free_fft(struct fft *fft);
void fft_start(struct fft *fft, int n, int samplerate);

void fft_resize(struct fft *fft, struct subwin *sw);
void sw_fft_redraw(struct subwin *sw, int flags);  
void fft_do(struct fft *fft);
int fft_pal(double amp, double min, double max);

#endif
#endif
