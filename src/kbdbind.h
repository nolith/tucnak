/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __KBDBIND_H
#define __KBDBIND_H

#include "header.h"
struct event;

#define KM_MAIN     0
#define KM_EDIT     1
#define KM_MENU     2
#define KM_MAX      3

enum {
    ACT_AC_CQ,
	ACT_AC_INFO,
    ACT_BACKSPACE,
    ACT_CALLINFO,
    ACT_CHOP, 
    ACT_CLEAR_TMPQSOS,
    ACT_CLEAR_TMPQSOS_INPUTLINE,
    ACT_CONFIRM_CALL,
    ACT_CONFIRM_EXC,
    ACT_CONFIRM_WWL,
    ACT_COPY_CLIPBOARD,
    ACT_CQ_0,
    ACT_CQ_1,
    ACT_CQ_2,
    ACT_CQ_3,
    ACT_CQ_4,
    ACT_CQ_5,
    ACT_CUT_CLIPBOARD,
    ACT_DELETE,
    ACT_DOWN,
    ACT_END,
    ACT_ENTER,
    ACT_ESC,
    ACT_FILE_MENU,
    ACT_FIND_NEXT,
    ACT_FIND_NEXT_BACK,
	ACT_FULLSCREEN,
    ACT_GRAB_BAND,
    ACT_HOME,
    ACT_INSERT,
    ACT_KILL_LINE,
    ACT_KILL_TO_BOL,
    ACT_KILL_TO_EOL,
    ACT_LASTCALL_KST,
    ACT_LEFT,
	ACT_MAXIMIZE,
    ACT_MENU,
    ACT_MODE,
    ACT_NEXT_HISTORY,
    ACT_NEXT_SUBWIN,  
    ACT_NEWCALL_KST,
	ACT_OPEN_NET,
    ACT_PAGE_DOWN,
    ACT_PAGE_UP,
    ACT_PASTE_CLIPBOARD,
    ACT_PREV_HISTORY, 
    ACT_PREV_SUBWIN,
	ACT_PLAY_LAST,
    ACT_QUIT,
    ACT_REALLYQUIT,
    ACT_RIGHT,
    ACT_ROTAR,
	ACT_RUNMODE,
    ACT_RX,
    ACT_RXTX, 
    ACT_SAVE_ALL,
/*    ACT_SCROLL_DOWN,*/
    ACT_SCREENSHOT,
    ACT_SCROLL_LEFT,
    ACT_SCROLL_RIGHT,
/*    ACT_SCROLL_UP,*/
    ACT_SEARCH,
    ACT_SEARCH_BACK,
    ACT_SEEK_A,
    ACT_SEEK_B,
    ACT_SHOW_HISTORY,
    ACT_SKED,
    ACT_SKED_QRG,
    ACT_SWAP_CALL,
    ACT_SWAP_WWL,
/*    ACT_TX, */
    ACT_TOGGLE_SPLIT_VFO,
    ACT_TUNE,
    ACT_UNFINISHED,
    ACT_UP,
    ACT_WINDOWSHOT,
	ACT_ZOOM0,
	ACT_ZOOMIN,
	ACT_ZOOMOUT
};

void init_keymaps(void);
void free_keymaps(void);
int kbd_action(int, struct event *);


#endif
