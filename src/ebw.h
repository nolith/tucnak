/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __EBW_H
#define __EBW_H

#include "header.h"



struct namedb;
struct cw;

#ifdef Z_MSC
#pragma pack(push, 1)
#endif

struct dbfhdr{
    char ver; /* mostly 03 */
    char year,month,day;
    guint32 records;
    guint16 hdrsize, recsize;
    char padding[20];
    /* total 32 bytes */
}__attribute__((packed));


struct dbffield{
    char name[11]; /* \0 terminated */
    char type; /* Char,Date YYYYMMDD,Float,Logical TFYN,Memo,Numeric */ 
    guint32 disaplacement;
    char len, decimal;
    char padding[14];
    /* total 32 bytes */
}__attribute__((packed));

#ifdef Z_MSC
#pragma pack(pop)
#endif

int load_ebw_from_file(struct cw *cw, struct namedb *namedb, char *filename);
int read_ebw_files(struct cw *cw, struct namedb *namedb);




#endif
