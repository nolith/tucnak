/*
    Tucnak - VHF contest log
    Copyright (C) 2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __SNDPIPE_H
#define __SNDPIPE_H

#include "header.h"
#include "dsp.h"

int sndpipe_open(struct dsp *dsp, int rec);
int sndpipe_close(struct dsp *dsp);
int sndpipe_write(struct dsp *dsp, void *data, int frames);
int sndpipe_read(struct dsp *dsp, void *data, int frames);
int sndpipe_reset(struct dsp *dsp);
int sndpipe_sync(struct dsp *dsp);
#ifdef HAVE_SNDFILE
int sndpipe_set_format(struct dsp *dsp, SF_INFO *sfinfo, int rec);
#endif
int sndpipe_set_sdr_format(struct dsp *dsp, int frames, int speed, int rec);


#endif
