
/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __CWWINDOW_H
#define __CWWINDOW_H

#include "header.h"
#include "bfu.h"

struct cwwin_data{
    int x,y,w,h;
    GString *text;
    int speed;
};

void cwwindow_func(struct window *win, struct event *ev, int fwd);


#endif
