/*
    Tucnak - VHF contest log
    Copyright (C) 2002-2022  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"
#include "language2.h"
#include "main.h"
#include "wizz.h"
#include "tsdl.h"

#define STORE_WIZZ_INT(name) \
    else if (strcasecmp(k, #name)==0){ \
        wi->name = atoi(item); \
    } \

#define STORE_WIZZ_ENUM(name, type) \
    else if (strcasecmp(k, #name)==0){ \
        wi->name = (type)atoi(item); \
    } \

#define STORE_WIZZ_STR(name) \
    else if (strcasecmp(k, #name)==0){ \
        if (wi->name) g_free(wi->name); \
        wi->name = g_strdup(item); \
    } \

struct wizz *wizz;

struct wizz *init_wizz(void){
    struct wizz *wizz;

	progress(VTEXT(T_INIT_WIZZ));		

    wizz = g_new0(struct wizz, 1);
    wizz->items = g_ptr_array_new();
    return wizz;
};

void free_wizz_item(struct wizz_item *wi){
    
    if (wi->tname) g_free(wi->tname);
    if (wi->pexch) g_free(wi->pexch);
    if (wi->excname) g_free(wi->excname);
    if (wi->bands) g_free(wi->bands);
    for (struct wizz_qsomult_item *wqi = wi->qsomults; wqi != NULL; ){
        struct wizz_qsomult_item *tmp = wqi;
        wqi = wqi->next;
        g_free(tmp);
    }

    g_free(wi);
}


void free_wizz(struct wizz *wizz){
    int i;
    struct wizz_item *wi;

	progress(VTEXT(T_FREE_WIZZ));		

    for (i=wizz->items->len-1; i>=0; i--){
        wi = (struct wizz_item *)g_ptr_array_index(wizz->items, i);
        free_wizz_item(wi);
    }
    
    g_ptr_array_free(wizz->items, TRUE);  /* YES, TRUE */
    g_free(wizz);
}

struct wizz_item *find_wizz(struct wizz *wizz, gchar *str){
    struct wizz_item *wi;
    int i;

    for (i=0; i<wizz->items->len; i++){
        wi = (struct wizz_item *)g_ptr_array_index(wizz->items, i);
        if (!wi->tname) continue;
        if (strcasecmp(wi->tname, str)==0) return wi;
    }
    return NULL;
}

int load_wizz_from_file(struct wizz *wizz, gchar *filename){
    FILE *f;
    GString *gs;
    struct wizz_item *wi=NULL;
    gchar **items, *k, *item;
    long int pos;

    
    f=NULL;
    pos=0;
    /*dbg("load_wizz_from_file('%s')\n", filename);*/
    if (filename){
        f = fopen(filename, "rt");
        if (!f) {
    /*        dbg("Can't open '%s'\n", filename);*/
            return -1;
        }
    }
    
    gs = g_string_sized_new(100);
    
    while(1){
        if (filename){
            if (!zfile_fgets(gs, f, 1)) break;
        }else{
            if (!zfile_mgets(gs, txt_tucnakwiz, &pos, sizeof(txt_tucnakwiz), 1)) break;
        }
        g_strstrip(gs->str);
    
        if (gs->str[0]=='['){
            gchar *tname;
            
                
            tname = g_strdup(gs->str+1);
            if (strlen(tname)>0 && tname[strlen(tname)-1]==']')
                tname[strlen(tname)-1]='\0';
            
            wi = find_wizz(wizz, tname);
            if (!wi) {
                wi = g_new0(struct wizz_item,1);
                g_ptr_array_add(wizz->items, wi);
                wi->wwlcfm = 1;
                wi->exccfm = 1;
            }

            /*dbg("BEGIN '%s'\n", gs->str+1);*/
            zg_free0(wi->tname);
            wi->tname = tname;  
/*            dbg("  set tname='%s' \n", tname);*/
            continue;
        }
        items=g_strsplit(gs->str, "=", 2);
        
        if (!items[0] || !items[1]) {
            g_strfreev(items);
            continue;
        }
        k=items[0];
        item=items[1];
        
        /*dbg("   '%s'='%s'\n", k, item);*/
		if (strlen(k) > 8 && strncasecmp(k, "qsomult_", 8) == 0){
			struct wizz_qsomult_item *wqi = g_new0(struct wizz_qsomult_item, 1);
			wqi->bandchar = z_char_uc(k[8]);
			wqi->qsomult = atoi(item);
			wqi->next = wi->qsomults;
			wi->qsomults = wqi;
		}
        STORE_WIZZ_STR(pexch)
        STORE_WIZZ_INT(rstused)
        STORE_WIZZ_INT(qsoused)
        STORE_WIZZ_INT(qsomult)
		STORE_WIZZ_INT(qsoglob)
		STORE_WIZZ_INT(expmode)
        STORE_WIZZ_ENUM(tttype, enum tttype)
        STORE_WIZZ_INT(wwlused)
        STORE_WIZZ_INT(wwlbonu)
        STORE_WIZZ_INT(wwlmult)
        STORE_WIZZ_INT(wwlcfm)
        STORE_WIZZ_INT(excused)
        STORE_WIZZ_INT(excbonu)
        STORE_WIZZ_INT(excmult)
        STORE_WIZZ_INT(exccfm)
        STORE_WIZZ_ENUM(exctype, enum exctype)
        STORE_WIZZ_STR(excname)
        STORE_WIZZ_INT(rstused)
        STORE_WIZZ_INT(defrstr)
        STORE_WIZZ_INT(prefmult)
        STORE_WIZZ_INT(prefglob)
        STORE_WIZZ_INT(dxcmult)
        STORE_WIZZ_INT(dxcbonu)
        STORE_WIZZ_INT(qsop_method)
        STORE_WIZZ_INT(total_method)
        STORE_WIZZ_STR(bands)
        else{
            dbg("!!! unresolved key '%s'\n", k);
        }
        g_strfreev(items);
    }

    if (filename) fclose(f);
    g_string_free(gs, 1);
    return 0;
}


void read_wizz_files(struct wizz *wizz){
    gchar *s;   
    int ret;
    
	progress(VTEXT(T_LOAD_WIZZ_DATA));		

    s = g_strconcat(tucnak_dir, "/tucnakwiz", NULL); 
	z_wokna(s);
    ret=load_wizz_from_file(wizz, s);
    g_free(s);
    if (ret<0){
        load_wizz_from_file(wizz, NULL);
    }
}

struct wizz_item *get_wizz(struct wizz *wizz, int i){
    return (struct wizz_item *) g_ptr_array_index(wizz->items, i);
}

