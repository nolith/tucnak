/*
    fft.c - fast fourier transform using libfftw3
    Copyright (C) 2009-2015 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include "header.h"

#ifdef USE_SDR

#include "fft.h"
#include "language2.h"
#include "rc.h"
#include "tsdl.h"
#include "subwin.h"

#define sqr(x) ((x)*(x))       
/* 
    VE3NEA:

    Gain := (1 - Exp(-(0.2 * NewSpectrum[i])));
    FilteredSpectrum[i] := FilteredSpectrum[i] * (1-Gain) + NewSpectrum[i] * Gain;
*/


struct fft *gfft;

#ifdef BLUEPAL
int bluepal[3*256] = {
    0,0,0,
    23,23,65,
    29,29,68,
    36,36,71,
    40,40,74,
    43,43,77,
    46,46,80,
    50,50,82,
    51,51,85,
    55,55,87,
    56,56,89,
    59,59,92,
    60,60,94,
    63,63,96,
    64,64,98,
    65,65,99,
    68,68,101,
    69,69,103,
    70,70,105,
    72,72,106,
    74,74,108,
    74,74,110,
    75,75,111,
    78,78,113,
    78,78,114,
    79,79,116,
    80,80,117,
    82,82,118,
    83,83,120,
    84,84,121,
    86,86,122,
    87,87,124,
    88,88,125,
    88,88,126,
    89,89,127,
    91,91,128,
    92,92,129,
    92,92,131,
    93,93,132,
    94,94,133,
    96,96,134,
    97,97,135,
    97,97,136,
    97,97,137,
    98,98,138,
    100,100,139,
    101,101,140,
    102,102,141,
    102,102,142,
    103,103,142,
    105,105,143,
    105,105,144,
    106,106,145,
    106,106,146,
    107,107,147,
    109,109,147,
    110,110,148,
    110,110,149,
    111,111,150,
    111,111,150,
    112,112,151,
    114,114,152,
    114,114,153,
    115,115,153,
    115,115,154,
    116,116,155,
    117,117,155,
    117,117,156,
    119,119,156,
    120,120,157,
    120,120,158,
    120,120,158,
    121,121,159,
    123,123,159,
    124,124,160,
    124,124,160,
    125,125,161,
    125,125,161,
    126,126,162,
    128,128,162,
    128,128,163,
    129,129,163,
    129,129,164,
    129,129,164,
    130,130,165,
    131,131,165,
    133,133,165,
    133,133,166,
    134,134,166,
    134,134,167,
    135,135,167,
    135,135,167,
    137,137,168,
    138,138,168,
    139,139,168,
    139,139,169,
    139,139,169,
    140,140,169,
    142,142,169,
    142,142,170,
    143,143,170,
    143,143,170,
    143,143,170,
    144,144,171,
    145,145,171,
    147,147,171,
    147,147,171,
    148,148,171,
    148,148,172,
    148,148,172,
    149,149,172,
    151,151,172,
    152,152,172,
    152,152,172,
    153,153,172,
    153,153,172,
    153,153,173,
    154,154,173,
    156,156,173,
    157,157,173,
    157,157,173,
    157,157,173,
    158,158,173,
    158,158,173,
    160,160,173,
    161,161,173,
    162,162,173,
    162,162,173,
    162,162,173,
    163,163,173,
    163,163,173,
    165,165,173,
    166,166,173,
    166,166,173,
    166,166,172,
    167,167,172,
    168,168,172,
    168,168,172,
    170,170,172,
    171,171,172,
    171,171,172,
    171,171,172,
    172,172,171,
    174,174,171,
    174,174,171,
    175,175,171,
    176,176,171,
    176,176,170,
    176,176,170,
    177,177,170,
    179,179,170,
    179,179,169,
    180,180,169,
    180,180,169,
    180,180,168,
    181,181,168,
    182,182,168,
    184,184,168,
    184,184,167,
    185,185,167,
    185,185,166,
    185,185,166,
    186,186,166,
    188,188,165,
    189,189,165,
    189,189,164,
    190,190,164,
    190,190,164,
    190,190,163,
    191,191,163,
    193,193,162,
    194,194,162,
    194,194,161,
    194,194,161,
    195,195,160,
    195,195,160,
    196,196,159,
    198,198,159,
    199,199,158,
    199,199,157,
    199,199,157,
    200,200,156,
    200,200,156,
    202,202,155,
    203,203,154,
    204,204,154,
    204,204,153,
    204,204,152,
    205,205,152,
    205,205,151,
    207,207,150,
    208,208,149,
    208,208,149,
    208,208,148,
    209,209,147,
    211,211,146,
    211,211,146,
    212,212,145,
    213,213,144,
    213,213,143,
    213,213,142,
    214,214,141,
    216,216,140,
    216,216,139,
    217,217,139,
    217,217,138,
    218,218,137,
    218,218,136,
    219,219,135,
    221,221,134,
    221,221,132,
    222,222,131,
    222,222,130,
    223,223,129,
    223,223,128,
    225,225,127,
    226,226,126,
    226,226,125,
    227,227,123,
    227,227,122,
    228,228,121,
    228,228,119,
    230,230,118,
    231,231,117,
    231,231,115,
    231,231,114,
    232,232,112,
    233,233,111,
    233,233,109,
    235,235,108,
    236,236,106,
    236,236,104,
    236,236,103,
    237,237,101,
    239,239,99,
    239,239,97,
    240,240,95,
    241,241,93,
    241,241,91,
    241,241,89,
    242,242,87,
    244,244,84,
    245,245,82,
    245,245,79,
    245,245,77,
    246,246,74,
    246,246,71,
    247,247,67,
    249,249,64,
    250,250,60,
    250,250,56,
    250,250,51,
    251,251,45,
    255,0,0,
    255,255,0,
    0,255,0,
};
#endif

struct fft *init_fft(void){
    struct fft *fft = NULL;
#ifdef ZIAPAL
    int f, r;
#endif

	int i, j;
#ifdef Z_HAVE_SDL	
    if (!sdl) 
#endif
		return NULL;
#ifdef Z_HAVE_SDL    

	progress(VTEXT(T_INIT_FFT));		

    fft = g_new0(struct fft, 1);
    fft->n = -1;
    fft->dynrin = NULL;
    
	j=0;
#ifdef ZIAPAL
	for (i = 128; i < 256; i++){
		r = (i-128)*2;
		f = 255 - r;
		fft->pal[j++] = z_makecol(0, 0, r);
	} 
	for (i = 256; i < 512; i++){
		r = i - 256;
		f = 255 - r;
		fft->pal[j++] = z_makecol(0, r, 255);
	}
	for (i = 512; i < 768; i++){
		r = i - 512;
		f = 255 - r;
		fft->pal[j++] = z_makecol(r, 255, f);
	}
	for (i = 768; i < 1024-32; i++){
		r = i - 768;
		f = 255 - r;
		fft->pal[j++] = z_makecol(255, f, 0);
	}
	if (j != FFT_COLORS) {
		//dbg("j=%d\n", j);
		zinternal("bad fft color number, please fix");
	}
#endif
#ifdef BLUEPAL
    for (i = 0; i < 256; i++) fft->pal[i] = z_makecol(bluepal[i * 3], bluepal[i * 3 + 1], bluepal[i * 3 + 2]);
#endif
#ifdef Z_HAVE_SDL
    MUTEX_INIT(fft->screen);
#endif    
#endif

    return fft;
}

void free_fft(struct fft *fft){
    if (!fft) return;
   	progress(VTEXT(T_FREE_FFT));		


    fftw_destroy_plan(fft->plan);
    fftw_free(fft->allocrin);
    fftw_free(fft->alloccout);

	g_free(fft->amp);
#ifdef Z_HAVE_SDL    
    if (fft->screen) SDL_FreeSurface(fft->screen);
    MUTEX_FREE(fft->screen);
#endif    

    g_free(fft);
}

void fft_start(struct fft *xyzzz, int n, int samplerate){
    //dbg("fft_start(%d)\n", n);
    if (!xyzzz) return;
    //dbg("fft_start(%d) ch=%d\n", n, dsp->channels);
	if (n != xyzzz->n){ 
     //   dbg("    realloc %d->%d\n", fft->n, n);
		xyzzz->n = n;
		if (xyzzz->allocrin){
			fftw_destroy_plan(xyzzz->plan);
			fftw_free(xyzzz->allocrin);
			fftw_free(xyzzz->alloccout);
			g_free(xyzzz->amp);
		}
		/*fft->rin = (double *) fftw_malloc(sizeof(double) * fft->n + 256); // fftw_malloc seems not to add padding elements
		fft->cout = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * fft->n + 256);*/

		xyzzz->lenrin = xyzzz->n;
		xyzzz->sizerin = xyzzz->lenrin * sizeof(double);
    	xyzzz->allocrin = (double *) fftw_malloc(xyzzz->sizerin); // fftw_malloc seems not to add padding elements
		xyzzz->dynrin = xyzzz->allocrin;

		xyzzz->lencout = xyzzz->n;
		xyzzz->sizecout = (xyzzz->lencout + 2) * sizeof(fftw_complex);
		xyzzz->alloccout = (fftw_complex *) fftw_malloc(xyzzz->sizecout);
		xyzzz->dyncout = xyzzz->alloccout;

    	xyzzz->plan = fftw_plan_dft_r2c_1d(xyzzz->n, xyzzz->dynrin, xyzzz->dyncout, FFTW_ESTIMATE);

		xyzzz->amplen = n;
		xyzzz->amp = g_new0(double, n * 10 /*xyzzz->amplen*/);			 
	}

#ifdef Z_HAVE_SDL
    xyzzz->screeny = 0;
#endif
    xyzzz->samplerate = samplerate;
    //memset(fft->ma_amp, 0, sizeof(fft->ma_amp));
#ifdef USE_NEA
    for (i=0; i<FFT_AMPLEN; i++) fft->nea_amp[i] = 0.0;				  
#endif

}

void fft_resize(struct fft *fft, struct subwin *sw){
#ifdef Z_HAVE_SDL    
    //dbg("fft_resize\n");
    SDL_Surface *newscr, *oldscr;
    //fft->wf_x2 = 260;
    //fft->wf_x3 = 2*260;

    int minh = sw->h * FONT_H / 2;
    sw->fft_sp_h = 210;
    if (sw->fft_sp_h > minh) sw->fft_sp_h = minh;
    sw->fft_sp_y = sw->h * FONT_H - sw->fft_sp_h;
    sw->fft_wf_y = 0;
    sw->fft_wf_h = sw->fft_sp_y - FONT_H - 10;
    //sw->fft_sp_x2 = fft->wf_x2;
    //sw->fft_wf_x2 = fft->wf_x2;

	//newscr = SDL_CreateRGBSurfaceWithFormat(SDL_SWSURFACE, sw->w*FONT_W, sw->h*FONT_H, zsdl->depth, zsdl->format);
    newscr = SDL_CreateRGBSurface(SDL_SWSURFACE, sw->w*FONT_W , sw->h*FONT_H, sdl->bpp, 
        sdl->screen->format->Rmask, sdl->screen->format->Gmask, sdl->screen->format->Bmask, 0);
    MUTEX_LOCK(fft->screen);
    oldscr = fft->screen;
    fft->screen = newscr;
    fft->screeny = 0;
    MUTEX_UNLOCK(fft->screen);
    if (oldscr) SDL_FreeSurface(oldscr);
#endif    
}

//#define F2PX(hz) (((hz) * gfft->n) / (cfg->ssbd_samplerate))
#define F2PX(hz) (((hz) * gfft->screen->w * 2) / (gfft->samplerate))

void sw_fft_redraw(struct subwin *sw, int flags){ 
#ifdef Z_HAVE_SDL    
    //dbg("sw_fft_redraw\n");
    int i, x = 0, y, sp_y, amp, x2, oldx;
    SDL_Rect src, dst;

    MUTEX_LOCK(gfft->screen);
    if (gfft->screeny <= 0){
        MUTEX_UNLOCK(gfft->screen);
        return;
    }
    y = gfft->screeny;
    gfft->screeny = 0;


    MUTEX_LOCK(sw->screen);

    // move old screen
    src.x = 0;
    src.y = sw->fft_wf_y + y;
    src.w = sw->screen->w;
    src.h = sw->fft_wf_h - y;
    dst.x = 0;
    dst.y = sw->fft_wf_y;
    dst.w = sw->screen->w;
    dst.h = sw->fft_wf_h - y;
    SDL_BlitSurface(sw->screen, &src, sw->screen, &dst);
    
    // add new data
    src.x = 0;
    src.y = 0;
    src.w = gfft->screen->w;
    src.h = y;
    dst.x = 0;
    dst.y = sw->fft_wf_y + sw->fft_wf_h - y;
    dst.w = sw->screen->w;
    dst.h = y;
    SDL_BlitSurface(gfft->screen, &src, sw->screen, &dst);

    // clear background under spectral lines
    dst.x = 0;
    dst.w = sw->screen->w;
    dst.y = sw->fft_sp_y;
    dst.h = sw->fft_sp_h;
    SDL_FillRect(sw->screen, &dst, 0);
    
    for (i = 100; i <= gfft->samplerate / 2; i += 100){
        x = F2PX(i);
        if (x > sw->w * FONT_W - 1) break;
        z_line(sw->screen, x, sw->fft_wf_h + 2, x, sw->fft_wf_h + 5, sdl->gr[10]);
    }
    x2 = x; 
    for (i = 0; i <= gfft->samplerate / 2; i += 1000){
        x = F2PX(i);
        if (x > sw->w * FONT_W - 1) break;
        z_line(sw->screen, x, sw->fft_wf_h + 1, x, sw->fft_sp_y - FONT_H , sdl->gr[10]);
		zsdl_printf(sw->screen, x, sw->fft_sp_y - FONT_H, sdl->gr[10], 0, ZFONT_CENTERX, "%dk", i / 1000);
        
    }
    z_line(sw->screen, F2PX(0), sw->fft_wf_h + 1, x2, sw->fft_wf_h + 1, sdl->gr[10]); 
        

    // draw spetral lines
    sp_y = sw->fft_sp_y + sw->fft_sp_h - 1;
    oldx = -1;
    for (i=0; i <= gfft->amplen; i++){
        x = (i * gfft->screen->w) / (gfft->n / 2);
        amp = gfft->amp[i] * (sw->fft_sp_h - 3);
        /*line(sw->screen, i, sp_y, i, sp_y - amp, sdl->gr[12]);

        amp = fft->ma_sum[i] * (sw->fft_sp_h - 3);
        line(sw->screen, i+fft->wf_x2, sp_y, i+fft->wf_x2, sp_y - amp, sdl->gr[12]);
          */
        //amp = fft->nea_amp[i] * (sw->fft_sp_h - 3);
        //line(sw->screen, i+fft->wf_x3, sp_y, i+fft->wf_x3, sp_y - amp, sdl->gr[12]);
        for (; oldx <= x; oldx++)
            z_line(sw->screen, oldx, sp_y, oldx, sp_y - amp, sdl->gr[12]);
    }
    MUTEX_UNLOCK(sw->screen);
    MUTEX_UNLOCK(gfft->screen);
    for (i = 0; i < FFT_COLORS; i++){
        z_line(sw->screen, i, 0, i, 5, gfft->pal[i]);
    }
#endif    
}

//int xxi=0x100;

void fft_do(struct fft *fft){
    int i, col, x, oldx; //mini, maxi, oldcol;
    double d, e;
//    double mind, maxd, mine, maxe, minv, maxv;
    double gain;
//    double min, max;
    //dbg("fft_do(n=%d)\n", fft->n);

    //mini = 200000000;
    //maxi = -200000000;
   /* mind = 1e30;
    maxd = -1e30;
    mine = 1e30;
    maxe = -1e30;
    minv = 1e30;
    maxv = -1e30;   */
#if 0
    for (i=0; i<fft->n; i++){
        fft->dynrin[i]=0.6 * cos((i* 1000.0 *2*M_PI)/cfg->ssbd_samplerate) +
                    0.1 * cos((i* 2000.0 *2*M_PI)/cfg->ssbd_samplerate) +
                    0.1 * cos((i* 3000.0 *2*M_PI)/cfg->ssbd_samplerate) +
                    //0.1 * cos((i* 4000.0 *2*Y_PI)/cfg->ssbd_samplerate) +
                    0;
       
    }
#endif  
#if 0
    for (i=0; i<fft->n; i++){
        fft->rin[i] *= 0.9;
        fft->rin[i] += 0.1 * cos((i* 1000.0 *2*MY_PI)/cfg->ssbd_samplerate);
    }
#endif  
#ifdef Z_HAVE_SDL    
    if (!fft->screen) return;
#endif
    
    fftw_execute(fft->plan);

//    dbg("%f\t%f\t%f\t%f\t%f\t%f\n", fft->cout[0], fft->cout[1], fft->cout[2], fft->cout[3], fft->cout[4], fft->cout[5]);
/*    for (i=0; i<6;i++){
        dbg("fft->cout=%f+%fi\t", fft->cout[i][0], fft->cout[i][1]);
    }
    dbg("\n");
    zinternal(""); */

/*    for (i=0; i<fft->n; i++){
        fft->cout[i]=256;
    }*/

    //min = DBL_MAX;
    //max = DBL_MIN;
#ifdef Z_HAVE_SDL    
    MUTEX_LOCK(fft->screen);
    //dbg("fft->n/2=%d  fft->screen->w=%d\n", fft->n/2, fft->screen->w);
    oldx = -1;
    for (i = 0; i <= fft->n / 2; i++){
        x = (i * fft->screen->w) / (fft->n / 2);
        if (x > fft->screen->w) break;
                        
        d = fabs(sqrt(sqr(fft->dyncout[i][0])+sqr(fft->dyncout[i][1])));
        d = d/(fft->n/2);  
        // d is epsilon to 1.0
        e = log10(d);
//        printf("%3d: %f\t%f\n", i, d, e);
        e -= log10(1.0/32768.0);
//        printf("e=%f %f\n", e, (log10(1.0)-log(1.0/32768.0)));
        e /= -log10(1.0/32768.0);
        if (e<0) e = 0;  // slabsi signaly nez 1 bit
        //e /= -log10(1.0/32768.0);
//        col = (int)e * FFT_COLORS;

/*        if (d<mind) mind = d;
        if (d>maxd) maxd = d;
        if (d<mine) mine = e;
        if (d>maxe) maxe = e; */

        /*if (col < 0) col = 0;
        if (col >= FFT_COLORS) col = FFT_COLORS - 1;*/
//            col = xxi;
		if (i >= fft->amplen) break; // can happen after resize    zinternal("fft->amp[%d] access (FFT_AMPLEN=%d)", i, FFT_AMPLEN);
        fft->amp[i] = e;
        col = (int)(e * (FFT_COLORS - 1.0));
        //col = i < FFT_COLORS?i:FFT_COLORS-1;
//        dbg("d=%11.9f e=%f\n", d, e);
//        dbg("col=%d\n");
//        if (col<mini) mini=col;
//        if (col>maxi) maxi=col;
        for (; oldx <= x; oldx++)
            z_putpixel(fft->screen, oldx, fft->screeny, fft->pal[col]);

        //fft->ma_amp[fft->ma_i][i] = e;
        /*fft->ma_sum[i] = 0;
        for (j=0; j<FFT_MA; j++) fft->ma_sum[i] += fft->ma_amp[j][i];
        fft->ma_sum[i] /= FFT_MA;
        col = fft->ma_sum[i] * (FFT_COLORS - 1);
        fast_putpixel(fft->screen, i+fft->wf_x2, fft->screeny, fft->pal[col]); */

/*        gain = fft->nea_amp[i];
        gain *= (-log10(1.0/32738.0));
        gain += log10(1.0/32768.0);
        gain *= log(10.0);
        gain = exp(gain);
        if (fft->nea_first) gain = 1;
        fft->nea_first = 0;          */
        gain = d;
        if (gain < 0.03) gain = 0.03;

//        dbg("gain=%f\n", gain);
#ifdef USE_NEA
        fft->nea_amp[i] = fft->nea_amp[i] * (1-gain) + e * gain;
        if (fft->nea_amp[i] > max) max = fft->nea_amp[i];
        if (fft->nea_amp[i] < min) min = fft->nea_amp[i]; 
#endif
    }    
    for (i=0; i<=fft->n/2; i++){
        if (i > fft->screen->w) break;
//        fast_putpixel(fft->screen, i, fft->screeny, fft_pal(fft->nea_amp[i], min, max));
    }
/*       xxi++;
       xxi %= FFT_COLORS;*/
    fft->screeny++;
    //fft->ma_i++;
    //fft->ma_i %= FFT_MA;
    if (fft->screeny == fft->screen->h) fft->screeny = 0;
    MUTEX_UNLOCK(fft->screen);
#endif
//    dbg("v=%f .. %f   \td=%f .. %f     \te=%f .. %f     \tcol=%d .. %d\n", minv, maxv, mind, maxd, mine, maxe, mini, maxi);
//    zinternal("");
    //dbg("col=%d..%d\n", mini, maxi);
}

int fft_pal(double amp, double min, double max){
	double norm;

    //dbg("min=%f max=%f\n", min, max);
    if (min == max) return gfft->pal[0];
    norm = (amp - min) / (max - min);    // 5 - 4 / 7 - 4
    if (norm < 0) norm = 0;
    if (norm > 1) norm = 1;
    return gfft->pal[(int)(norm*(FFT_COLORS-3))];
}
#endif
