/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2015 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __BFU_H
#define __BFU_H

#include <zlist.h>

#include "header.h" 
#include "language2.h" // here is no needed but for other modules

#define AL_LEFT     0
#define AL_CENTER   1

#define AL_MASK     0x7f

#define AL_EXTD_TEXT    0x80
#define AL_BUTTONS  0x100


struct memory_list {
    int n;
    void *p[1];
};

struct memory_list *getml(void *, ...);
void add_to_ml(struct memory_list **, ...);
void freeml(struct memory_list *);

#define MENU_FUNC (void (*)(void *, void *))
#define CALLBACK_FUNC (void (*)(void *))

extern char m_bar;

#define M_BAR   (&m_bar)

struct menu;

struct menu_item {
    char *text;
    char *rtext;
    char *hotkey;
    void (*func)(void *, void *);
    void *data;
    int in_m; /* 0 menu zmizi po vyberu */
    int free_i;
	int checkbox;
	int checked;
	void (*funcall)(struct menu *menu);
};

struct menu {
    int selected;
    int view;
    int xp, yp;
    int x, y, xw, yw;
    int ni;
    void *data;
    struct window *win;
    struct menu_item *items;
	char *title;
};

struct mainmenu {
    int selected;
    int sp;
    int ni;
    void *data;
    struct window *win;
    struct menu_item *items;
};

struct history_item {
    struct history_item *next;
    struct history_item *prev;
    char d[1];
};

struct history {
    int n;
    struct list_head items;
};

#define D_END       0
#define D_CHECKBOX  1
#define D_FIELD     2
#define D_FIELD_PASS    3
#define D_BUTTON    4
#define D_BOX       5
#define D_CHECKBOX3 6
#define D_TEXT		7

#define B_ENTER     1
#define B_ESC       2

struct dialog_item_data;
struct dialog_data;

struct dialog_item {
    int type;
    int gid, gnum; /* for buttons: gid - flags B_XXX */ /* for fields: min/max */ /* for box: gid is box height */
    int (*fn)(struct dialog_data *, struct dialog_item_data *);
    struct history *history;
    int dlen;           /* data length */
    int maxl;
    char *data;
    void *udata;        /* for box: holds list */
    char *text;
    char *msg;
    int wrap;
    int align;
    int tabcompl, tabi;
};

struct dialog_item_data {
    int x, y, l;
    int vpos, cpos;
    int checked;
    struct dialog_item *item;
    struct list_head history;
    struct history_item *cur_hist;
    char *cdata;
};

#define EVENT_PROCESSED     0

struct dialog_data;
struct event; 

struct dialog {
    char *title;
    void (*fn)(struct dialog_data *);
    void (*fn2)(struct dialog_data *);
    void (*position_fn)(struct dialog_data *);
    int (*handle_event)(struct dialog_data *, struct event *);
    void (*abort)(struct dialog_data *);
    void *udata;
    void *udata2;
    int align;
    void (*refresh)(void *);
    void *refresh_data;
    int y0;
    struct dialog_item items[1];
};

struct dialog_data {
    struct window *win;
    struct dialog *dlg;
    int x, y, xw, yw;
    int n;
    int selected;
    struct memory_list *ml;
    int min, max, yy, w, rw, ofs;
    struct dialog_item_data items[1];
};

/* Which fields to free when zapping a box_item. Bitwise or these. */
enum box_item_free {NOTHING = 0, TEXT = 1 , DATA = 2};
/* An item in a box */
struct box_item {
    struct box_item *next;
    struct box_item *prev;
    char *text;    /* Text to display */
    void *data; /* data */
    enum box_item_free free_i;
};

/* Stores display information about a box. Kept in cdata. */
struct dlg_data_item_data_box {
    int sel;    /* Item currently selected */   
    int box_top;    /* Index into items of the item that is on the top line of the box */
    /*struct list_head items;*/
    struct box_item items; /* The list being displayed */
    int list_len;   /* Number of items in the list */
};


void menu_func(struct window *, struct event *, int);
void mainmenu_func(struct window *, struct event *, int);
void dialog_func(struct window *, struct event *, int);

struct menu_item *new_menu(int);
struct menu_item *add_to_menu(struct menu_item **mi, char *text, char *rtext, char *hotkey, void (*func)(void *, void *), void *data, int in_m);
void do_menu(struct menu_item *items, void *data); /* todo data=ses */
void do_menu_selected(struct menu_item *, void *data, int);
void do_mainmenu(struct menu_item *, void *, int);
void do_dialog(struct dialog *, struct memory_list *);
int check_number(struct dialog_data *, struct dialog_item_data *);
int check_hex(struct dialog_data *, struct dialog_item_data *);
int check_double(struct dialog_data *, struct dialog_item_data *);
int check_qrg(struct dialog_data *, struct dialog_item_data *);
//int check_spowe(struct dialog_data *, struct dialog_item_data *);
int check_nonempty(struct dialog_data *, struct dialog_item_data *);
void max_text_width(struct terminal *, char *, int *);
void min_text_width(struct terminal *, char *, int *);
void dlg_format_text(struct terminal *, struct terminal *, char *, int, int *, int, int *, int, int);
void max_buttons_width(struct terminal *, struct dialog_item_data *, int, int *);
void min_buttons_width(struct terminal *, struct dialog_item_data *, int, int *);
void dlg_format_buttons(struct terminal *, struct terminal *, struct dialog_item_data *, int, int, int *, int, int *, int);
void dlg_format_buttons1(struct terminal *, struct terminal *, struct dialog_item_data *, int, int, int *, int, int *, int);
void checkboxes_width(struct terminal *, char **, int *, void (*)(struct terminal *, char *, int *));
void dlg_format_checkbox(struct terminal *, struct terminal *, struct dialog_item_data *, int, int *, int, int *, char *);
void dlg_format_checkboxes(struct terminal *, struct terminal *, struct dialog_item_data *, int, int, int *, int, int *, char **);
void dlg_format_field(struct terminal *, struct terminal *, struct dialog_item_data *, int, int *, int, int *, int);
void max_group_width(struct terminal *, char **, struct dialog_item_data *, int, int *);
void min_group_width(struct terminal *, char **, struct dialog_item_data *, int, int *);
void dlg_format_group(struct terminal *, struct terminal *, char **, struct dialog_item_data *, int, int, int *, int, int *);
void dlg_format_group1(struct terminal *, struct terminal *, char **, struct dialog_item_data *, int, int, int *, int, int *);
void dlg_format_box(struct terminal *, struct terminal *, struct dialog_item_data *, int, int *, int, int *, int);
void checkbox_list_fn(struct dialog_data *);
void group_fn(struct dialog_data *);
void center_dlg(struct dialog_data *);
void draw_dlg(struct dialog_data *);
void display_dlg_item(struct dialog_data *, struct dialog_item_data *, int);
int ok_dialog(struct dialog_data *, struct dialog_item_data *);
int cancel_dialog(struct dialog_data *, struct dialog_item_data *);
void msg_box(struct memory_list *, char *, int, ...);
void input_field_fn_i(struct dialog_data *, int i);
void input_field_fn(struct dialog_data *);
void input_field(struct memory_list *, char *, char *, char *, char *, void *,struct history *, int, char *, int, int, int (*)(struct dialog_data *, struct dialog_item_data *), void (*)(void *, char *), void (*)(void *), int tabcompl);
void add_to_history(struct history *, char *);

void box_sel_move(struct dialog_item_data *, int ); 
void show_dlg_item_box(struct dialog_data *, struct dialog_item_data *);
void box_sel_set_visible(struct dialog_item_data *, int ); 
void errbox(char *text, int errcode, ...); 
void menu_save_rc(void *arg);


void dlg_pf_init(int phase, struct dialog_data *dlgd);
void dlg_pf_group(int phase, struct dialog_data *dlgd, int cnt);
void dlg_pf_buttons(int phase, struct dialog_data *dlgd, int cnt);
void dlg_pf_fn(struct dialog_data *dlgd);

int do_tab_compl(struct dialog_item_data *di, int tabi);

void show_dlg_item_box(struct dialog_data *, struct dialog_item_data *); 

#endif
