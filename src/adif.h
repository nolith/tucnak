/*
    Tucnak - VHF contest log
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ADIF_H
#define __ADIF_H

int export_all_bands_adif(void);
void import_adifx(char *filename);
void import_adif(void *xxx, char *filename);

#endif
