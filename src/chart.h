/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __CHART_H
#define __CHART_H

#include "subwin.h"
#include "qsodb.h"

#define CHART_H 0

struct chqso{
    int time;
    int value;
    char *call;
    int pts;
    char *op;
};

struct chband{
    GPtrArray *chqsos; // of struct chqso
    int color;
    char *pcall;
    char *pwwlo;
    char *tname;
    char *pband;
    int mouse_value;
    char *filename;
	struct band *band;
    char *op;  // foreign key to chqsos->op;
};

int  sw_chart_load_file(struct subwin *sw, char *filename, int quiet);
int  sw_chart_kbd_func(struct subwin *sw, struct event *ev, int fw);
int  sw_chart_mouse_func(struct subwin *sw, struct event *ev, int fw);
void sw_chart_redraw(struct subwin *sw, struct band *band, int flags);
void sw_chart_check_bounds(struct subwin *sw);
void sw_chart_raise(struct subwin *sw);
int save_chart_to_file(char *filename);
int load_chart_from_file(char *filename);

void sw_chart_free(struct subwin *sw);
void sw_chart_free_band(struct chband *b);
int sw_chart_recalc_extremes(struct subwin *sw, struct band *band);
int chart_reload(void);
void chart_clear_all(void);
void sw_chart_delete_band(void *idx, void *unused);

void menu_chart_add_contest(void *menudata, void *itdata);
void menu_chart_add_files(void *menudata, void *itdata);


#endif
