/*
    Tucnak - VHF contest log
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZOSK_H
#define __ZOSK_H

#include "tsdl.h"


#ifdef Z_HAVE_SDL
struct zosk_key{
    int x, y, w, h, cx, cy;
	int isshift, pressed;
	SDL_Keycode sym, symFn, symShift;
    char text[5], textFn[5], textShift[5];
};

struct zosk{
	SDL_Surface *surface;
    GPtrArray *keys;
    int bw2, bh, x0, y0, x2, y;
    int KX2, KY;
	int font_w, font_h;
	struct zosk_key *active, *sent;
	int bg, bbg;
	int parent_x, parent_y, screen_w;
	int shifts_active;
	struct zosk_key *shift, *ctrl, *alt, *fn, *esc;
	GString *gs;
	int maxlen_c;
	int flags;
	int repeat_timer_id;
};

#define ZOSK_ENTERCLOSE 1  // enter or esc close
#define ZOSK_UPCONVERT  2
#define ZOSK_SENDENTER  4
#define ZOSK_CLEARENTER 8
#define ZOSK_IGNOREESC  16

struct zosk *zosk_init(SDL_Surface *screen, int flags, const char *text);
void zosk_free(struct zosk *zosk);

void zosk_draw(struct zosk *zosk);
int zosk_click(struct zosk *zosk, int x, int y, int pressed);
void zosk_clear(struct zosk *zosk);
void zosk_portrait(struct zosk *zosk);

#endif


#endif
