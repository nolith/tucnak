/*
    Tucnak - VHF contest log
	Functions for mingw32
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/


#include "header.h"

#ifdef __MINGW32__

int init_mingw(void){
	WSADATA wsaData;

//    mingw_stderr = fopen("_stderr", "wt");
//    if (!mingw_stderr) zinternal("Can't open file _stderr");

    //MessageBox(NULL, "Hello World, I'm Tucnak compiled under mingw", "Info", MB_OK);
//	MUTEX_INIT(strerror_r);
//	MUTEX_INIT(strtok_r);
//	MUTEX_INIT(gmtime_r);

	WSAStartup(MAKEWORD(2, 0), &wsaData);
}

/*int WINAPI WinMain1(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
    char **items;
    int argc;
    char *argv[100];

    init_mingw();
    items = g_strsplit(lpCmdLine, " ", 0);
    argv[0]="tucnak.exe" FIXME;
    for (argc=1; items[argc-1] != 0; argc++){
        argv[argc] = items[argc-1];
    }
    main(argc, argv);
    g_strfreev(items);
} */


#endif
