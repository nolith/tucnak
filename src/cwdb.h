/*
    Tucnak - VHF contest log
    Copyright (C) 2011-2022 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __CWDB_H
#define __CWDB_H

#include "header.h"


struct band;
struct contest;
extern struct cw *cw;  
  
struct cw_item{
   gchar *wwl0, *wwl1;    
   gint stamp0, stamp1;
   gint qrv;                /* bit array, lsb = A = 50MHz etc. Since 1.15 */
};

struct wc_item{
   gchar *call0, *call1;    
   gint stamp0, stamp1;
};

struct cw{
    ZHashTable *cw; /* key=call, value=cw_item */
    GHashTable *wc;
    int latest;
    int minstamp;
	int dirty;
};

struct qs{
    GPtrArray *result1;
    gchar *str;
}; 

struct cw *init_cw(void);
void free_cw(struct cw *cw);

gint get_cw_size(struct cw *cw);
gint get_wc_size(struct cw *cw);

void load_one_cw(struct cw *cw, gchar *s);
int load_cw_from_file(struct cw *cw, gchar *filename);
void read_cw_files(struct cw *cw);
int save_cw_string(struct cw *cw, GString *gs);
int save_cw_into_file(struct cw *cw, gchar *filename);
int save_cw(struct cw *cw, int verbose);

void add_cw(struct cw *cw, gchar *call, gchar *wwl, gint stamp, gchar *qrv);
gchar *find_wwl_by_call(struct cw *cw, gchar *call);
gchar *find_wwl_by_call_newer(struct cw *cw, gchar *call, int minstamp);

void add_wc(struct cw *cw, gchar *wwl, gchar *call, gint stamp);
gchar *find_call_by_wwl(struct cw *cw, gchar *wwl);

gchar *find_qrv_str_by_call(struct cw *cw, gchar *call);

//int compare_gstring(const void *a, const void *b);

void update_cw_from_band(struct cw *cw, struct band *band);
void update_cw_from_ctest(struct cw *cw, struct contest *ctest);

void get_cw_qs(gchar *str);
gpointer cw_thread_func(gpointer data);
int cmp_cw_qs(gchar *call, struct cw_item *cwi, GPtrArray *result);
void qs_thread_create(void);
void qs_thread_join(void);
void qs_thread_kill(void);
int get_susp_ambiguous_call(struct cw *cw, struct band *band, char *call, char *wwl, GString *gs, int thr);
int similar_calls(const char *call1, const char *call2, int factor, int thr, int p);
int cwdb_call_info(GString *gs, gchar *call, gchar *stroke, GHashTable *wwls);
int load_cw_from_mem(struct cw *cw, const char *file, size_t len);
int format_cw_string(struct cw *cw, GString *gs);


#endif
