#!/bin/bash

# This script creates package for use on Asus EEE (Xandros)
# Must be run on Debian Etch or older system
# On EEE simply download and unpack file in home directory
# then press Ctrl+Alt+T to start console and run tucnak/tucnak

set -e

cd ../libzia
./configure --without-ftdi
make

cd ../tucnak
#./configure --without-gpm --without-ftdi --without-hamlib --without-fftw3
./configure --without-gpm --with-pkg=eee
make

VER=`make ver`

rm -rf eee
mkdir eee
cd eee
mkdir tucnak
cp ../src/tucnak tucnak

sed 's/gfx_x *=.*/gfx_x           = 792/' <../data/tucnakrc >1
sed 's/gfx_y *=.*/gfx_y           = 416/' <1 >2
sed 's/loglines *=.*/loglines        = 1/' <2 >3
sed 's/cwda_type *=.*/cwda_type     = 0/' <3 >tucnak/tucnakrc

F="tucnak3-eee-$VER.tar.gz"
tar czf ../$F tucnak
echo $F created
cd ..
rm -rf eee
