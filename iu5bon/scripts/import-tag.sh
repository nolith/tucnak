#!/bin/bash

##############################################################
### import-tag.sh - tucnak tarball to git utility          ###
### Author: Alessio Caiazza IU5BON                         ###
### Helper script to import tucnak/libzia release tarballs ###
###  into my git mirror.                                   ###
##############################################################

set -e

tag=$1
if [ "$tag" == "" ]; then
    echo "Usage: $0 <tag>"
    exit 1
fi

tucnak_src=$(pwd)
tmp_folder=$(mktemp -d -t tucnak)

function clone_import_tag {
    src=$1
    project=$(basename "$src")

    pushd "$tmp_folder"
    git clone "$src" --branch main "$project"
    cd "$project"
    git ls-files ':!:iu5bon/*' | xargs rm

    curl -o - "http://tucnak.nagano.cz/$project-$tag.tar.gz" | tar zxf - --strip-components=1

    changelog="../$project.changelog"

    "$tucnak_src/iu5bon/scripts/extract_changelog.sh" > "$changelog" || echo "Import v$tag" > "$changelog"

    less "$changelog"

    read -r -p "Continue? [y/n] " -s -n 1 ans
    echo
    [ "$ans" != "y" ] && exit 1

    git add ./*
    git diff --stat --cached
    read -r -p "Press any key to continue" -s -n 1

    git diff --cached

    read -r -p "Continue? [y/n] " -s -n 1 ans
    echo
    [ "$ans" != "y" ] && exit 1

    git commit --author "OK1ZIA <ok1zia@nagano.cz>" -F "$changelog"
    git tag "$tag"

    if [ "$TEST" == "1" ]; then
        echo "dry-run mode not pushing"
        exit 0

    fi
    remote=$(git --git-dir "$src/.git" remote get-url --push origin)
    git push "$remote" main:main "$tag"
    git --git-dir "$src/.git" fetch origin

    popd
}

echo "Creating a temporary folder: $tmp_folder"

clone_import_tag "$tucnak_src/../libzia"
clone_import_tag "$tucnak_src"

rm -rf "$tmp_folder"
