#/bin/bash

set -e

version=$(sed -n 's/^=\(.*\)=.*/\1/p' ChangeLog | head -1)

if [ -z "${version}" ] ; then
    echo "Cannot extract a version from changelog"
    exit 1
fi

sed "/^=${version}/ d" ChangeLog | sed '/^=/,$ d' 
