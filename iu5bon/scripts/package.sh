#!/bin/bash

set -e

if [ "$CI" == "true" ]; then
    echo "Running in CI"

    gem install fpm git
fi

VERSION=$(git describe  --always --tags)

fpm \
  -s dir -t deb \
  -p "tucnak-$VERSION-armhf.deb" \
  --name tucnak \
  --version "$VERSION" \
  --architecture armhf \
  --depends libglib2.0-0 \
  --depends libgnutls30 \
  --depends libgtk-3-0 \
  --depends libpng16-16 \
  --depends libsndfile1 \
  --depends libftdi1 \
  --depends libasound2 \
  --depends libfftw3-double3 \
  --depends libfftw3-single3 \
  --depends libportaudio2 \
  --depends librtlsdr0 \
  --depends libhamlib4 \
  --description "Tucnak is multiplatform VHF/HF contest logbook" \
  --url "http://tucnak.nagano.cz/wiki/Main_Page" \
  --maintainer "Alessio Caiazza - IU5BON" \
  build/=/usr
  

