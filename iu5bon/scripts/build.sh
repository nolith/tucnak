#!/bin/bash

set -e

if [ "$CI" == "true" ]; then
    echo "Running in CI"

    src_dir=$(pwd)
    cd ../
    git clone --depth 1  --branch="$CI_COMMIT_REF_SLUG" https://gitlab.com/nolith/libzia.git || git clone --depth 1 https://gitlab.com/nolith/libzia.git
fi

export MAKEFLAGS=-j$(grep -c ^processor /proc/cpuinfo)

cd libzia
./configure
make


cd "$src_dir"

./configure --prefix="$src_dir/build"
make
make install
