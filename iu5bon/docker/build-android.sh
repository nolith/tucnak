#!/bin/bash

set -e

if [ "$CI" == "true" ]; then
    echo "Running in CI"
    export HOME=/home/ja
    export USER=ja
    source "$HOME/.profile"

    src_dir=$(pwd)
    cd /home/ja/c
    cp -r "$src_dir" tucnak
    git clone --depth 1  --branch="$CI_COMMIT_REF_SLUG" https://gitlab.com/nolith/libzia.git || git clone --depth 1 https://gitlab.com/nolith/libzia.git
fi

export MAKEFLAGS=-j$(grep ^processor /proc/cpuinfo | wc -l)

pushd /home/ja/c

cd tucnak
echo "*********************** PATCHES **************************"
PATCHES="iu5bon/patches/*.patch"
for p in $PATCHES; do
  if [ -f "$p" ]; then
    echo "*** apply $p"
    patch -b -p1 < "$p"
  elif [ "$p" == "$PATCHES" ]; then
    echo "No patches to apply"
  else
    echo "Warning: Some problem with \"$p\""
  fi
done
cd ..

echo "*********************** libzia configure *****************"
cd libzia
./configure

cd android

echo "*********************** GLIB *****************************"
./glib-compile

echo "*********************** libsndfile ***********************"
./libsndfile-compile

echo "*********************** libzia ***************************"
./libzia-compile

echo "************************ tucnak **************************"
cd ../../tucnak
./configure

cd android
./tucnak-compile
./tucnak-package

[ "$CI" == "true" ] && cp -r bin "$CI_PROJECT_DIR/android/"
popd
