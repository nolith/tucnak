FROM registry.gitlab.com/nolith/tucnak/toolchain

RUN apt update && apt upgrade -y && \
    apt install -y build-essential \
    default-jdk file git automake \
    pkg-config ant

# the toolchain is 32bit, let's install the needed libraries
RUN apt install -y lib32z1 lib32ncurses5 lib32stdc++6

ENV PATH=$ANDROID_SDK/platform-tools:$ANDROID_SDK/tools:$ANDROID_NDK:$PATH
WORKDIR /home/ja/c

# libzia/tucnak deps
RUN apt install -y libglib2.0-dev binutils-dev libgnutls28-dev libiconv-hook-dev
