#!/usr/bin/perl

# Convert TACLog C_W file Text format to tucnak C_W file format
# Copyleft � 2008 Dave Gilligan, G1OGY. <g1ogy_|at|_g1ogy.com>

#############################################################################
# This program is free software; you can redistribute it and/or modify it   
# under the terms of the GNU General Public License as published by the 
# Free Software Foundation; either version 2 of the License, or (at your    
# option) any later version. 
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
# Public License for more details. 
#   http://www.m1cro.org.uk/Members/G1OGY/GNU-GPL.html
#
#     Succinctly: If it breaks - you get to keep all the pieces.
#
#############################################################################


$prog = "TAC2tuc";
$version = "V 0.01";
$datestring="[20080713]";
$auth = "Author:G1OGY <g1ogy!*at*!g1ogy.com> ";
$arights="Licenced under GNU GPL.";

# define Usage

    die "\n\"$prog\"  $version           $datestring     $auth
        $arights
  Usage:
     $prog  inputfile.ext  [outputfile.ext]  
     
    \"$prog\" requires an INPUT file name argument and optionally, 
    an OUTPUT file name.
    The user must have previously converted the TACLog .C_W file to
    text-mode using C_W.exe (see the TACLog manual).
    A raw column format of: 
        CALL  LOC  LOC2   DATE
    is expected, separated by \"WhiteSpace\".  
    Even though the TACLog formating of the text input to C_W.EXE is
    critical, for this program the amount of \"WhiteSpace\" is 
    unimportant; thus this program will deal with any list otherwise 
    derived.    
    But NO blank lines nor Column Headers are allowed!
    
    Note that there is no way to determine a valid QSO date for LOC2.
    Consequently, the QSO Date for LOC2 will be set to 12 December 1901
    (for want of a more auspicious occasion) thus ensuring that LOC 
    takes place-precedence over LOC2.
    

    Default OUTPUT file is \"tac2tuc.cw\" in the `current` directory.\n"
    
unless @ARGV;


# define local variables

    my ($rawfile)=$ARGV[0];
    my ($yesterdate)="19011212";

# main 

    Banner();

    if ($ARGV[1] ne "")  {
        open (C_WFORMAT,">$ARGV[1]") || die "\nCannot Create Output file: \"$ARGV[1]\"!\n";
        }
        else  { 
            open (C_WFORMAT,">tac2tuc.cw") || die "\nCannot Create Output file: \"tac2tuc.cw\"!\n"; 
            }
    open (RAWLIST, "$rawfile") || die "\nInput File not Found\! Check the path : \n$!";

    print "\nWorking...\n";
    while (<RAWLIST>) {

# read the input and write a delimited output to an array	

        chomp;
        ($call,$loc,$loc2,$qsodate) = split;
        
# deal with call loc date 
		unless ($qsodate)	{
			unless ($loc2 =~ m/\D^*/i)	{
				$qsodate=$loc2;
				$loc2="";
			}
		}


# deal with single/twin locators and add the delimiter

		if ($loc2 =~ m/\D^*/i)	{
			push (@returndata, ($call.";".$loc.";".$qsodate));
 			push (@returndata, ($call.";".$loc2.";".$yesterdate));
 		}
 			else	{
 				push (@returndata, ($call.";".$loc.";".$qsodate));
			}

	}        

# break out into individual vars again to write the correctly 
# formatted output file

      	foreach $record (@returndata)	{
     		($call,$loc,$qsodate) = split (/;/,$record);
       			write (C_WFORMAT);
        }		
   
# clean up
    close (RAWLIST); close (C_WFORMAT);


# output format 

format C_WFORMAT =
@<<<<<<<<<<<<<<@<<<<<<@<<<<<<<
$call,$loc,$qsodate
.
        
# end_of_format

Credits();

    ############################################################################################
    # Banner
    
    sub Banner  {
        print ("\n\"$prog\"  $version  $datestring  $auth\n   $arights\n");
        print ("\n  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
        }
    
    ############################################################################################
    # Credits
    
    sub Credits {
        
        print ("\n  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");       
        print ("    Credits: ");
        print ("\n      \"$prog\" is written against \"ActivePerl v5.6\" and compiled \n");
        print ("        - where relevant - with \"Perl2exe\" by Indigostar Software.\n\n"); 
        print ("        \"$prog\": GNU General Public Licence applies.\n");
        print ("\n");
        print ("\n  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
        }
    
    ############################################################################################


__END__