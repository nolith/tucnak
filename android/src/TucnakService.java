package cz.nagano.tucnak;

import android.app.Service;
import android.os.IBinder;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;

public class TucnakService extends Service{
	static int NOTIFY_ID = 1236709873; // Random ID

    public TucnakService(){
		System.out.println("tucnak: TucnakService()");
    }

    @Override
    public void onCreate() {
		System.out.println("tucnak: TucnakService.onCreate()");
    }

    public void start(){
		System.out.println("tucnak: TucnakService.start()");
		
		Intent intent = new Intent(this, MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
		Notification n = new Notification(R.drawable.icon, "Tucnak is running", System.currentTimeMillis());
		n.setLatestEventInfo(this, "Tucnak", "Tucnak is running, click to activate", pendingIntent);
        startForeground(NOTIFY_ID, n);
		System.out.println("tucnak: TucnakService.service started");
    }
    public void stop(){
		System.out.println("tucnak: TucnakService.stop()");
        stopForeground(true);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
		System.out.println("tucnak: TucnakService.onStartCommand()");
        start();
        return(START_NOT_STICKY);
    }

    @Override 
    public IBinder onBind(Intent intent) {
		System.out.println("tucnak: TucnakService.onBind()");
        return(null);
    }
}

